from unittest import TestCase
import datetime
import zoneinfo
from heaobject.storage import AWSS3Storage


class TestAWSS3Storage(TestCase):

    def setUp(self) -> None:
        self.__test_awsstorage = AWSS3Storage()

    def test_storage_bytes_initial_value(self):
        self.assertIsNone(self.__test_awsstorage.size)

    def test_storage_bytes_set_none(self):
        self.__test_awsstorage.size = None
        self.assertIsNone(self.__test_awsstorage.size)

    def test_storage_bytes_set_int(self):
        self.__test_awsstorage.size = 12345
        self.assertEqual(self.__test_awsstorage.size, 12345)

    def test_storage_bytes_set_float(self):
        self.__test_awsstorage.size = 12345.6789
        self.assertEqual(self.__test_awsstorage.size, 12345)

    def test_storage_bytes_set_str(self):
        self.__test_awsstorage.size = '12345'
        self.assertEqual(self.__test_awsstorage.size, 12345)

    def test_storage_bytes_set_not_supports_int(self):
        with self.assertRaises(ValueError,
                               msg='Setting storage bytes successful with object that cannot be converted to an int'):
            self.__test_awsstorage.size = 'asd123kkl'

    def test_object_count_initial_value(self):
        self.assertIsNone(self.__test_awsstorage.object_count)

    def test_object_count_set_int(self):
        self.__test_awsstorage.object_count = 1024
        self.assertEqual(self.__test_awsstorage.object_count, 1024)

    def test_object_count_set_str(self):
        self.__test_awsstorage.object_count = '1024'
        self.assertEqual(self.__test_awsstorage.object_count, 1024)

    def test_object_count_set_float(self):
        self.__test_awsstorage.object_count = 1024.801
        self.assertEqual(self.__test_awsstorage.object_count, 1024)

    def test_object_count_set_not_supports_int(self):
        with self.assertRaises(ValueError,
                               msg='Setting object count successful with object that cannot be converted to a int'):
            self.__test_awsstorage.object_count = 'asd123kkl'

    def test_object_init_modified_initial_value(self):
        self.assertIsNone(self.__test_awsstorage.object_earliest_created)

    def test_object_init_modified_set_none(self):
        self.__test_awsstorage.object_init_modified = None
        self.assertIsNone(self.__test_awsstorage.object_earliest_created)

    def test_object_init_modified_set_date(self):
        self.__test_awsstorage.object_earliest_created = datetime.date(2022, 4, 1)
        self.assertEqual(self.__test_awsstorage.object_earliest_created, datetime.date(2022, 4, 1))

    def test_object_init_modified_set_datetime(self):
        self.__test_awsstorage.object_earliest_created = datetime.datetime(2022, 4, 1, 10, 29, 31, tzinfo=datetime.timezone.utc)
        self.assertEqual(self.__test_awsstorage.object_earliest_created, datetime.datetime(2022, 4, 1, 10, 29, 31, tzinfo=datetime.timezone.utc))

    def test_object_init_modified_set_str(self):
        self.__test_awsstorage.object_earliest_created = '2022-04-01'
        self.assertEqual(self.__test_awsstorage.object_earliest_created, datetime.date(2022, 4, 1))

    def test_object_init_modified_set_str_year(self):
        self.__test_awsstorage.object_earliest_created = '2022'
        self.assertEqual(self.__test_awsstorage.object_earliest_created, datetime.date(2022, 1, 1))

    def test_object_init_modified_set_str_year_and_month(self):
        self.__test_awsstorage.object_earliest_created = '2022-2'
        self.assertEqual(self.__test_awsstorage.object_earliest_created, datetime.date(2022, 2, 1))

    def test_object_init_modified_set_invalid_str(self):
        with self.assertRaises(ValueError,
                               msg='Setting modified successful with string that cannot be converted to a date'):
            self.__test_awsstorage.object_earliest_created = 'as04012022'

    def test_object_last_modified_initial_value(self):
        self.assertIsNone(self.__test_awsstorage.object_last_modified)

    def test_object_last_modified_set_none(self):
        self.__test_awsstorage.object_last_modified = None
        self.assertIsNone(self.__test_awsstorage.object_last_modified)

    def test_object_last_modified_set_date(self):
        self.__test_awsstorage.object_last_modified = datetime.date(2022, 4, 1)
        self.assertEqual(self.__test_awsstorage.object_last_modified, datetime.date(2022, 4, 1))

    def test_object_last_modified_set_datetime(self):
        self.__test_awsstorage.object_last_modified = datetime.datetime(2022, 4, 1, 10, 29, 31, tzinfo=datetime.timezone.utc)
        self.assertEqual(self.__test_awsstorage.object_last_modified, datetime.datetime(2022, 4, 1, 10, 29, 31, tzinfo=datetime.timezone.utc))

    def test_object_last_modified_set_str(self):
        self.__test_awsstorage.object_last_modified = '2022-04-01'
        self.assertEqual(self.__test_awsstorage.object_last_modified, datetime.date(2022, 4, 1))

    def test_object_last_modified_set_invalid_str(self):
        with self.assertRaises(ValueError,
                               msg='Setting modified successful with string that cannot be converted to a date'):
            self.__test_awsstorage.object_last_modified = 'as04012022'

    def test_volume_id_initial_value(self):
        self.assertIsNone(self.__test_awsstorage.volume_id)

    def test_volume_id_set_none(self):
        self.__test_awsstorage.volume_id = None
        self.assertIsNone(self.__test_awsstorage.volume_id)

    def test_volume_id_set_str(self):
        self.__test_awsstorage.volume_id = '666f6f2d6261722d71757578'
        self.assertEqual(self.__test_awsstorage.volume_id, '666f6f2d6261722d71757578')

    def test_volume_id_set_int(self):
        self.__test_awsstorage.volume_id = 1234567890
        self.assertEqual(self.__test_awsstorage.volume_id, '1234567890')
