from unittest import TestCase
from heaobject import registry, folder, root, keychain


class TestGetAttributes(TestCase):

    def test_get_folder_attributes(self):
        expected = {'source', 'source_detail', 'name', 'display_name', 'description', 'owner',
                    'created', 'modified', 'derived_by', 'derived_from', 'invites',
                    'shares', 'type', 'id', 'mime_type', 'path', 'type_display_name', 'instance_id'}
        self.assertEqual(expected, set(folder.Folder().get_attributes()))

    def test_get_registry_attributes(self):
        expected = {'source', 'source_detail', 'name', 'display_name', 'description', 'owner',
                    'created', 'modified', 'derived_by', 'derived_from', 'invites',
                    'shares', 'type', 'resources', 'base_url', 'external_base_url', 'id',
                    'type_display_name', 'instance_id'}
        self.assertEqual(expected, set(registry.Component().get_attributes()))

    def test_simple_member(self):
        class Foo(root.AbstractDesktopObject):
            def __init__(self):
                super().__init__()
                self.foo = 'bar'
        self.assertIn('foo', Foo().get_attributes())

    def test_class_variable(self):
        class Foo(root.AbstractDesktopObject):
            foo = 'bar'
        self.assertNotIn('foo', Foo().get_attributes())

    def test_keychain_attributes(self):
        expected = {'source', 'source_detail', 'name', 'display_name', 'description', 'owner',
                    'created', 'modified', 'derived_by', 'derived_from', 'invites',
                    'shares', 'type', 'id', 'type_display_name', 'instance_id',
                    'session_token', 'time_to_expire', 'account', 'expiration', 'role_arn',
                    'where', 'managed', 'password', 'lifespan_class', 'account_id', 'aws_role_name',
                    'lifespan', 'maximum_duration', 'state', 'role', 'temporary', 'for_presigned_url'}
        self.assertEqual(expected, set(keychain.AWSCredentials().get_attributes()))
