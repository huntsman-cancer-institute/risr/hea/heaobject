from unittest import IsolatedAsyncioTestCase
from heaobject.folder import Folder
from heaobject.user import NONE_USER, ALL_USERS
from heaobject.root import ShareImpl, Permission, PermissionContext, DesktopObject
from heaobject.organization import Organization
from functools import partialmethod


class TestPermission(IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.obj = Folder()
        self.obj.owner = NONE_USER

        share1 = ShareImpl()
        share1.user = 'reximus'
        share1.permissions = [Permission.EDITOR]
        share2 = ShareImpl()
        share2.user = 'luximus'
        share2.permissions = [Permission.SHARER]
        self.obj.shares = [share1, share2]
        context = PermissionContext[DesktopObject](sub=NONE_USER)

    async def test_is_owner(self):
        assert await self.obj.has_permissions([Permission.COOWNER], PermissionContext[DesktopObject](sub=NONE_USER))

    async def test_has_editor(self):
        assert await self.obj.has_permissions([Permission.EDITOR], PermissionContext[DesktopObject](sub='reximus'))

    async def test_does_not_have_editor(self):
        assert not await self.obj.has_permissions([Permission.EDITOR], PermissionContext[DesktopObject](sub='luximus'))

    async def test_has_sharer(self):
        assert await self.obj.has_permissions([Permission.SHARER], PermissionContext[DesktopObject](sub='luximus'))

    async def test_does_not_have_sharer(self):
        assert not await self.obj.has_permissions([Permission.SHARER], PermissionContext[DesktopObject](sub='reximus'))

    async def test_none_sub(self):
        with self.assertRaises(ValueError):
            await self.obj.has_permissions([Permission.COOWNER], PermissionContext[DesktopObject](sub=None))  # type: ignore

    async def test_none_permission(self):
        with self.assertRaises(ValueError):
            await self.obj.has_permissions(NONE_USER, PermissionContext[DesktopObject](sub=None))  # type: ignore

    async def test_get_permissions_none_user(self):
        self.assertCountEqual([p for p in Permission],
                              await self.obj.get_permissions(PermissionContext[DesktopObject](sub=NONE_USER)))

    async def test_get_permissions_reximus(self):
        self.assertEqual([Permission.EDITOR],
                         await self.obj.get_permissions(PermissionContext[DesktopObject](sub='reximus')))

    async def test_get_permissions_luximus(self):
        self.assertEqual([Permission.SHARER],
                         await self.obj.get_permissions(PermissionContext[DesktopObject](sub='luximus')))

    async def test_get_permissions_other(self):
        self.assertEqual([], await self.obj.get_permissions(PermissionContext[DesktopObject](sub='joe')))

    async def test_get_permissions_none(self):
        with self.assertRaises(ValueError):
            await self.obj.get_permissions(PermissionContext[DesktopObject](sub=None))  # type: ignore


class TestOrganizationPermission(IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.org = Organization()
        self.org.owner = NONE_USER
        self.org.add_admin_id('joe')
        self.org.principal_investigator_id = 'bob'
        self.org.add_manager_id('reximus')
        self.org.add_member_id('luximus')

        share = ShareImpl()
        share.user = ALL_USERS
        share.permissions = [Permission.CHECK_DYNAMIC]
        self.org.add_share(share)

    async def test_get_permissions_manager(self):
        self.assertCountEqual([Permission.VIEWER, Permission.EDITOR, Permission.SHARER],
                              await self.org.get_permissions(PermissionContext[DesktopObject](sub='reximus')))

    async def test_get_permissions_member(self):
        self.assertCountEqual([Permission.VIEWER, Permission.SHARER],
                              await self.org.get_permissions(PermissionContext[DesktopObject](sub='luximus')))

    async def test_has_check_dynamic_permissions_manager(self):
        self.assertTrue(await self.org.has_permissions([Permission.VIEWER, Permission.CHECK_DYNAMIC], PermissionContext[DesktopObject](sub='reximus')))

    async def test_has_viewer_permissions_manager(self):
        self.assertTrue(await self.org.has_permissions([Permission.VIEWER], PermissionContext[DesktopObject](sub='reximus')))

    async def test_has_creator_permissions_manager(self):
        self.assertFalse(await self.org.has_permissions([Permission.CREATOR], PermissionContext[DesktopObject](sub='reximus')))

    async def test_has_editor_permissions_manager(self):
        self.assertTrue(await self.org.has_permissions([Permission.EDITOR], PermissionContext[DesktopObject](sub='reximus')))

    async def test_has_sharer_permissions_manager(self):
        self.assertTrue(await self.org.has_permissions([Permission.SHARER], PermissionContext[DesktopObject](sub='reximus')))

    async def test_has_deleter_permissions_manager(self):
        self.assertFalse(await self.org.has_permissions([Permission.DELETER], PermissionContext[DesktopObject](sub='reximus')))

    async def test_has_check_dynamic_permissions_member(self):
        self.assertTrue(await self.org.has_permissions([Permission.VIEWER, Permission.CHECK_DYNAMIC], PermissionContext(sub='luximus')))

    async def test_has_viewer_permissions_member(self):
        self.assertTrue(await self.org.has_permissions([Permission.VIEWER], PermissionContext[DesktopObject](sub='luximus')))

    async def test_has_editor_permissions_member(self):
        self.assertFalse(await self.org.has_permissions([Permission.EDITOR], PermissionContext[DesktopObject](sub='luximus')))

    async def test_has_sharer_permissions_member(self):
        self.assertTrue(await self.org.has_permissions([Permission.SHARER], PermissionContext[DesktopObject](sub='luximus')))

    async def test_is_admin_ids_editable_for_admin_user(self):
        await self._is_admin_ids_editable(self.assertEqual, 'joe')

    async def test_is_admin_ids_editable_for_pi(self):
        await self._is_admin_ids_editable(self.assertNotEqual, 'bob')

    async def test_is_admin_ids_editable_for_manager(self):
        await self._is_admin_ids_editable(self.assertNotEqual, 'reximus')

    async def test_is_admin_ids_editable_for_member(self):
        await self._is_admin_ids_editable(self.assertNotEqual, 'luximus')

    async def test_is_manager_ids_editable_for_admin_user(self):
        await self._is_manager_ids_editable(self.assertEqual, 'joe')

    async def test_is_manager_ids_editable_for_pi(self):
        await self._is_manager_ids_editable(self.assertEqual, 'bob')

    async def test_is_manager_ids_editable_for_manager(self):
        await self._is_manager_ids_editable(self.assertEqual, 'reximus')

    async def test_is_manager_ids_editable_for_member(self):
        await self._is_manager_ids_editable(self.assertNotEqual, 'luximus')

    async def test_is_member_ids_editable_for_admin_user(self):
        await self._is_member_ids_editable(self.assertEqual, 'joe')

    async def test_is_member_ids_editable_for_pi(self):
        await self._is_member_ids_editable(self.assertEqual, 'bob')

    async def test_is_member_ids_editable_for_manager(self):
        await self._is_member_ids_editable(self.assertEqual, 'reximus')

    async def test_is_member_ids_editable_for_member(self):
        await self._is_member_ids_editable(self.assertNotEqual, 'luximus')

    async def _is_editable(self, attr, method, sub):
        return method([Permission.VIEWER, Permission.EDITOR], await self.org.get_attribute_permissions(attr, PermissionContext[DesktopObject](sub=sub)))

    _is_admin_ids_editable = partialmethod(_is_editable, 'admin_ids')
    _is_manager_ids_editable = partialmethod(_is_editable, 'manager_ids')
    _is_member_ids_editable = partialmethod(_is_editable, 'member_ids')
