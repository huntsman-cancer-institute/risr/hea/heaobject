from unittest import TestCase
from heaobject.source2target import Source2Target, Mapping


class TestSource2Target(TestCase):
    def setUp(self) -> None:
        self.__source2target = Source2Target()

    def test_mappings_ok(self) -> None:
        mappings = [Mapping(), Mapping()]
        self.__source2target.mappings = mappings
        self.assertEqual(mappings, self.__source2target.mappings)

    def test_mappings_copy(self) -> None:
        mappings = [Mapping(), Mapping()]
        self.__source2target.mappings = mappings
        self.assertIsNot(mappings, self.__source2target.mappings)

    def test_mappings_none(self) -> None:
        with self.assertRaises(ValueError):
            self.__source2target.mappings = None

    def test_mappings_not_all_mappings(self) -> None:
        with self.assertRaises(TypeError):
            self.__source2target.mappings = [Mapping(), 'foo', Mapping()]
