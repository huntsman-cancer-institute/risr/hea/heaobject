from unittest import TestCase

from heaobject.trash import AWSS3FolderFileTrashItem
from heaobject.awss3key import encode_key
from datetime import datetime


class TestAWSS3FolderFileTrashItem(TestCase):
    def setUp(self) -> None:
        self.item = AWSS3FolderFileTrashItem()

    def test_valid_original_location(self):
        try:
            self.item.original_location = '/hello/world/'
        except ValueError:
            self.fail()

    def test_relationship_with_original_location(self):
        self.item.key = 'world/'
        self.item.bucket_id = 'hello'
        self.assertEqual('/hello/world/', self.item.original_location)

    def test_relationship_with_original_location_no_bucket_id(self):
        self.item.key = 'world/'
        self.assertEqual(None, self.item.original_location)

    def test_weird_key_for_folder_with_no_name(self):
        self.item.key = '/'
        self.item.bucket_id = 'bucket'
        self.assertEqual('/bucket//', self.item.original_location)

    def test_key_for_original_location_for_folder_with_no_name(self):
        self.item.original_location = '/bucket//'
        self.assertEqual('/', self.item.key)

    def test_key_for_original_location_for_file(self):
        self.item.original_location = '/bucket/TextFile.txt'
        self.assertEqual('TextFile.txt', self.item.key)

    def test_original_location_for_id_and_bucket_id(self):
        self.item.id = encode_key('hello/TextFile.txt') + ',' + '1'
        self.item.bucket_id = 'my-bucket'
        self.assertEqual('/my-bucket/hello/TextFile.txt', self.item.original_location)

    def test_original_location_for_name_and_bucket_id(self):
        self.item.name = encode_key('hello/TextFile.txt') + ',' + '1'
        self.item.bucket_id = 'my-bucket'
        self.assertEqual('/my-bucket/hello/TextFile.txt', self.item.original_location)

    def test_display_name_for_name_and_bucket_id(self):
        self.item.name = encode_key('hello/TextFile.txt') + ',' + '1'
        self.item.bucket_id = 'my-bucket'
        self.assertEqual('TextFile.txt', self.item.display_name)

    def test_key_for_name_and_bucket_id(self):
        self.item.name = encode_key('hello/TextFile.txt') + ',' + '1'
        self.item.bucket_id = 'my-bucket'
        self.assertEqual('hello/TextFile.txt', self.item.key)

    def test_key_for_id_and_bucket_id(self):
        self.item.id = encode_key('hello/TextFile.txt') + ',' + '1'
        self.item.bucket_id = 'my-bucket'
        self.assertEqual('hello/TextFile.txt', self.item.key)

    def test_version_for_id(self):
        self.item.id = encode_key('hello/TextFile.txt') + ',' + '1'
        self.assertEqual('1', self.item.version)

    def test_version_for_name(self):
        self.item.name = encode_key('hello/TextFile.txt') + ',' + '1'
        self.assertEqual('1', self.item.version)

    def test_id_from_version(self):
        self.item.version = '1'
        self.item.key = 'hello/TextFile.txt'
        self.assertEqual('aGVsbG8vVGV4dEZpbGUudHh0,1', self.item.id)

    def test_actual_object_id_from_version(self):
        self.item.version = '1'
        self.item.key = 'hello/TextFile.txt'
        self.assertEqual('aGVsbG8vVGV4dEZpbGUudHh0', self.item.actual_object_id)

    def test_name_from_version(self):
        self.item.version = '1'
        self.item.key = 'hello/TextFile.txt'
        self.assertEqual('aGVsbG8vVGV4dEZpbGUudHh0,1', self.item.name)

    def test_deleted_string(self):
        now = datetime.now()
        self.item.deleted = now.isoformat()
        self.assertEqual(now, self.item.deleted)
