import unittest
from heaobject import awss3key


class BucketObjectKeyTest(unittest.TestCase):
    def test_replace_parent_folder(self):
        self.assertEqual('raz/baz/bar', awss3key.replace_parent_folder('foo/oof/bar', 'raz/baz/', 'foo/oof/'))

    def test_replace_parent_folder_recursive(self):
        self.assertEqual('raz/baz/foo/oof/bar', awss3key.replace_parent_folder('foo/oof/bar', 'raz/baz/', source_key_folder=''))

    def test_replace_folder_source(self):
        self.assertEqual('raz/baz/bar/', awss3key.replace_parent_folder('foo/oof/bar/', 'raz/baz/', 'foo/oof/'))

    def test_source_key_folder(self):
        self.assertEqual('oof/bar/baz', awss3key.replace_parent_folder('foo/bar/baz', 'oof/', 'foo/'))

    def test_source_key_folder_not_a_folder(self):
        with self.assertRaises(ValueError):
            self.assertEqual('oof/bar/baz', awss3key.replace_parent_folder('foo/bar/baz', 'oof/', 'foo'))

    def test_invalid_target_folder(self):
        with self.assertRaises(ValueError):
            awss3key.replace_parent_folder('foo/oof/bar', 'raz/baz', 'foo/oof/')

    def test_none_target_folder(self):
        self.assertEqual('bar', awss3key.replace_parent_folder('foo/oof/bar', None, 'foo/oof/'))

    def test_none_source_and_parent(self):
        self.assertEqual('raz/baz/', awss3key.replace_parent_folder(None, 'raz/baz/', None))

    def test_none_parent(self):
        self.assertEqual('raz/baz/foo', awss3key.replace_parent_folder('foo', 'raz/baz/', None))

    def test_none_source_and_a_parent(self):
        with self.assertRaises(ValueError):
            self.assertEqual('raz/baz/foo', awss3key.replace_parent_folder(None, 'raz/baz/', 'foo/'))

    def test_mismatched_source_and_parent(self):
        with self.assertRaises(ValueError):
            awss3key.replace_parent_folder('bar', 'raz/baz/', 'foo/')

    def test_none_none_none(self):
        self.assertEqual('', awss3key.replace_parent_folder(None, None, None))

    def test_join_none_head(self):
        self.assertEqual('foobar', awss3key.join(None, 'foobar'))

    def test_join_empty_string_head(self):
        self.assertEqual('foobar', awss3key.join('', 'foobar'))

    def test_parent(self):
        self.assertEqual('foo/', awss3key.parent('foo/bar'))

    def test_parent_root(self):
        self.assertEqual('', awss3key.parent('bar'))

    def test_parent_root_root(self):
        self.assertEqual('', awss3key.parent(None))

    def test_split_none(self):
        self.assertEqual(('', ''), awss3key.split(None))

    def test_key_in_folder(self):
        self.assertTrue(awss3key.is_object_in_folder('foo/bar/baz', 'foo/bar/'))

    def test_key_in_non_folder(self):
        self.assertFalse(awss3key.is_object_in_folder('foo/bar/baz', 'foo/bar'))

    def test_key_not_in_folder(self):
        self.assertFalse(awss3key.is_object_in_folder('foo/bar', 'foo/bar/baz/'))

    def test_key_in_root(self):
        self.assertTrue(awss3key.is_object_in_folder('foo', None))

    def test_key_in_root2(self):
        self.assertTrue(awss3key.is_object_in_folder('foo', ''))

    def test_root_in_root(self):
        self.assertTrue(awss3key.is_object_in_folder(None, None))


# joined, split
split_join_data = [
    ('hello/goodbye/foobar/', ('hello/goodbye/', 'foobar/')),
    ('hello/goodbye/foobar', ('hello/goodbye/', 'foobar')),
    ('foobar', ('', 'foobar')),
    ('/', ('', '/'))
]


def join_test_generator(joined, split):
    def test(self):
        self.assertEqual(joined, awss3key.join(*split))
    return test


def split_test_generator(joined, split):
    def test(self):
        self.assertEqual(split, awss3key.split(joined))
    return test


for d in split_join_data:
    join_test = join_test_generator(d[0], d[1])
    setattr(BucketObjectKeyTest, f'test_join_{d[1]}', join_test)
    split_test = split_test_generator(d[0], d[1])
    setattr(BucketObjectKeyTest, f'test_split_{d[0]}', split_test)


# original, encoded, is_folder
encode_decode_data = [('foobar', 'Zm9vYmFy', False),
                      ('foobar/', 'Zm9vYmFyLw==', True),
                      ('hello/goodbye/foobar/', 'aGVsbG8vZ29vZGJ5ZS9mb29iYXIv', True)]


def encode_test_generator(orig, encoded):
    def test(self):
        self.assertEqual(encoded, awss3key.encode_key(orig))
    return test


def decode_test_generator(orig, encoded):
    def test(self):
        self.assertEqual(orig, awss3key.decode_key(encoded))
    return test


def is_folder_test_generator(orig, is_folder):
    def test(self):
        self.assertEqual(is_folder, awss3key.is_folder(orig))
    return test


for d in encode_decode_data:
    join_test = encode_test_generator(d[0], d[1])
    setattr(BucketObjectKeyTest, f'test_encode_{d[0]}', join_test)
    split_test = decode_test_generator(d[0], d[1])
    setattr(BucketObjectKeyTest, f'test_decode_{d[0]}', split_test)
    is_folder_test = is_folder_test_generator(d[0], d[2])
    setattr(BucketObjectKeyTest, f'test_is_folder_{d[0]}', is_folder_test)

