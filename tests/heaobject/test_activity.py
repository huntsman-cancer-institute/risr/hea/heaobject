from unittest import TestCase
from heaobject.activity import Status, DesktopObjectAction
from typing import Literal


class TestDesktopObjectAction(TestCase):

    def setUp(self) -> None:
        self.test_activity = DesktopObjectAction()

    def test_user_id_initial_value(self):
        self.assertIsNone(self.test_activity.user_id)

    def test_user_id_set_none(self):
        self.test_activity.user_id = None
        self.assertIsNone(self.test_activity.user_id)

    def test_user_id_set_str(self):
        self.test_activity.user_id = '123456'
        self.assertEqual(self.test_activity.user_id, '123456')

    def test_user_id_set_int(self):
        self.test_activity.user_id = 1234
        self.assertEqual(self.test_activity.user_id, '1234')

    def test_action_initial_value(self):
        self.assertIsNone(self.test_activity.description)

    def test_action_set_none(self):
        self.test_activity.description = None
        self.assertIsNone(self.test_activity.description)

    def test_action_set_str(self):
        self.test_activity.description = 'update bucket'
        self.assertEqual(self.test_activity.description, 'update bucket')

    def test_action_set_arbitrary(self):
        self.test_activity.description = int
        self.assertEqual(self.test_activity.description, str(int))

    def test_status_initial_value(self):
        self.assertEqual(self.test_activity.status, Status.REQUESTED)

    def test_status_set_none(self):
        self.test_activity.status = None
        if self.test_activity.status != Status.REQUESTED:
            self.fail('Wrong default value')

    def test_status_complete_enum(self):
        self.test_activity.status = Status.SUCCEEDED
        self.assertEqual(self.test_activity.status, Status.SUCCEEDED)

    def test_status_complete_string(self):
        self.test_activity.status = Status.SUCCEEDED.name
        self.assertEqual(self.test_activity.status, Status.SUCCEEDED)

    def test_status_set_invalid_string(self):
        with self.assertRaises(ValueError,
                               msg='"dkafjdfk" is being considered a valid status'):
            self.test_activity.status = 'dkafjdfk'

def context_dependent_object_path_generator(a = Literal['old', 'new']):
    def test_context_dependent_object_path(self: TestDesktopObjectAction):
        expected = ['a', 'b', 'c']
        setattr(self.test_activity, f'{a}_context_dependent_object_path', expected)
        self.assertEqual(getattr(self.test_activity, f'{a}_context_dependent_object_path'), expected)
    setattr(TestDesktopObjectAction, f'test_{a}_context_dependent_object_path', test_context_dependent_object_path)

    def test_context_dependent_object_path_invalid(self: TestDesktopObjectAction):
        setattr(self.test_activity, f'{a}_context_dependent_object_path', [1])
        self.assertEqual(getattr(self.test_activity, f'{a}_context_dependent_object_path'), ['1'])
    setattr(TestDesktopObjectAction, f'test_{a}_context_dependent_object_path_invalid',
            test_context_dependent_object_path_invalid)

    def test_context_dependent_object_path_list_item_invalid(self: TestDesktopObjectAction):
        setattr(self.test_activity, f'{a}_context_dependent_object_path', ['a', 1])
        self.assertEqual(getattr(self.test_activity, f'{a}_context_dependent_object_path'), ['a', '1'])
    setattr(TestDesktopObjectAction, f'test_{a}_context_dependent_object_path_list_item_invalid',
            test_context_dependent_object_path_list_item_invalid)

    def test_context_dependent_object_path_None(self: TestDesktopObjectAction):
        setattr(self.test_activity, f'{a}_context_dependent_object_path', None)
        self.assertEqual(getattr(self.test_activity, f'{a}_context_dependent_object_path'), None)
    setattr(TestDesktopObjectAction, f'test_{a}_context_dependent_object_path_None',
            test_context_dependent_object_path_None)

context_dependent_object_path_generator('old')
context_dependent_object_path_generator('new')

