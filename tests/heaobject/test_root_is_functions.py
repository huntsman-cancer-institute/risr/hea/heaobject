from unittest import TestCase
from heaobject.root import is_primitive, is_primitive_list, is_desktop_object_dict, is_heaobject_type, is_desktop_object_type
from heaobject.aws import S3Object
from heaobject.registry import Component
from enum import Enum
from datetime import datetime, date
import collections


class TestRootIsFunctions(TestCase):
    def test_is_primitive_date(self):
        assert is_primitive(date(2021, 1, 1))

    def test_is_primitive_datetime(self):
        assert is_primitive(datetime.now())

    def test_is_primitive_int(self):
        assert is_primitive(2)

    def test_is_primitive_float(self):
        assert is_primitive(4.5)

    def test_is_primitive_str(self):
        assert is_primitive('foo')

    def test_is_primitive_enum(self):
        class test_enum(Enum):
            one = 1
            two = 2
            three = 3

        assert is_primitive(test_enum.one)

    def test_is_primitive_none_type(self):
        assert is_primitive(None)

    def test_is_primitive_false_dict(self):
        assert not is_primitive({})

    def test_is_primitive_false_heaobject_dict(self):
        assert not is_primitive({'type': 'heaobject.registry.Component'})

    def test_is_primitive_false_list(self):
        assert not is_primitive([])

    def test_is_primitive_list_of_primitives(self):
        class test_enum(Enum):
            one = 1
            two = 2
            three = 3
        assert is_primitive_list([1, "one", 2.5, True, None, test_enum.two])

    def test_is_primitive_list_false_set(self):
        assert not is_primitive_list(set())

    def test_is_primitive_list_false_str(self):
        assert not is_primitive_list('foo')

    def test_is_primitive_list_false_dict(self):
        assert not is_primitive_list({})

    def test_is_primitive_list_false_defaultdict(self):
        assert not is_primitive_list(collections.defaultdict())

    def test_is_primitive_list_false_heaobject_dict(self):
        assert not is_primitive_list({'type': 'heaobject.registry.Component'})

    def test_is_desktop_object_dict(self):
        assert is_desktop_object_dict({'type': 'heaobject.registry.Component'})

    def test_is_desktop_object_dict_false(self):
        assert not is_desktop_object_dict({'type': 'heaobject.registry.Resource'})

    def test_is_hea_object_dict(self):
        assert is_desktop_object_dict({'type': 'heaobject.registry.Component'})

    def test_is_obj_object_dict_2(self):
        assert not is_desktop_object_dict({'type': 'heaobject.registry.Resource'})

    def test_is_hea_object_dict_false(self):
        assert not is_desktop_object_dict({'name': 'heaobject.registry.Component'})

    def test_is_hea_object_dict_false_2(self):
        assert not is_desktop_object_dict(2)

    def test_is_heaobject_type(self):
        assert is_heaobject_type('heaobject.registry.Component')

    def test_is_not_heaobject_type(self):
        assert not is_heaobject_type('heaobject.registry.Components')

    def test_is_desktop_object_type(self):
        assert is_desktop_object_type('heaobject.registry.Component')

    def test_is_not_desktop_object_type(self):
        assert not is_desktop_object_type('heaobject.registry.Resource')

    def test_is_not_desktop_object_type_2(self):
        assert not is_desktop_object_type('list')

    def test_is_not_desktop_object_type_upper_bound(self):
        assert not is_desktop_object_type('heaobject.registry.Component', type_=S3Object)

    def test_is_desktop_object_type_upper_bound_2(self):
        assert is_desktop_object_type('heaobject.registry.Component', type_=Component)
