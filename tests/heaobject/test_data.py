from unittest import TestCase
from heaobject.data import DataFile, AWSS3FileObject
from heaobject.aws import *
from datetime import datetime


class TestDataFile(TestCase):
    def test_human_readable_size_b(self):
        d = DataFile()
        d.size = 134
        self.assertEqual('134 Bytes', d.human_readable_size)

    def test_human_readable_size_kb(self):
        d = DataFile()
        d.size = 134134
        self.assertEqual('134.1 kB', d.human_readable_size)

    def test_human_readable_size_mb(self):
        d = DataFile()
        d.size = 134134134
        self.assertEqual('134.1 MB', d.human_readable_size)

    def test_human_readable_size_gb(self):
        d = DataFile()
        d.size = 134134134134
        self.assertEqual('134.1 GB', d.human_readable_size)

    def test_human_readable_size_tb(self):
        d = DataFile()
        d.size = 134134134134134
        self.assertEqual('134.1 TB', d.human_readable_size)

    def test_human_readable_size_zero(self):
        d = DataFile()
        d.size = 0
        self.assertEqual('0 Bytes', d.human_readable_size)

    def test_human_readable_size_none(self):
        d = DataFile()
        self.assertIsNone(d.human_readable_size)


class TestAWSS3FileObject(TestCase):
    def setUp(self):
        self.s3_file_object = AWSS3FileObject()

    def test_storage_class_standard_and_retrievable(self):
        self.s3_file_object.storage_class = S3StorageClass.STANDARD
        self.assertTrue(self.s3_file_object.retrievable)

    def test_storage_class_glacier_and_retrievable(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.assertIsNone(self.s3_file_object.retrievable)

    def test_storage_class_glacier_and_archived_and_retrievable(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.ARCHIVED
        self.assertFalse(self.s3_file_object.retrievable)

    def test_storage_class_glacier_and_restoring_and_retrievable(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORING
        self.assertFalse(self.s3_file_object.retrievable)

    def test_storage_class_glacier_and_restored_and_retrievable(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORED
        self.assertTrue(self.s3_file_object.retrievable)

    def test_restored_and_retrievable(self):
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORED
        self.assertTrue(self.s3_file_object.retrievable)

    def test_storage_class_standard_and_available_until_test_storage_class(self):
        self.s3_file_object.storage_class = S3StorageClass.STANDARD
        self.s3_file_object.available_until = datetime.now()
        self.assertIsNone(self.s3_file_object.storage_class)

    def test_storage_class_standard_and_available_until_test_archive_detail_state(self):
        self.s3_file_object.storage_class = S3StorageClass.STANDARD
        self.s3_file_object.available_until = datetime.now()
        self.assertEqual(S3ArchiveDetailState.RESTORED, self.s3_file_object.archive_detail_state)

    def test_storage_class_glacier_and_archived_and_available_until(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.ARCHIVED
        self.s3_file_object.available_until = datetime.now()
        self.assertEqual(S3ArchiveDetailState.RESTORED, self.s3_file_object.archive_detail_state)

    def test_storage_class_glacier_and_restoring_and_available_until_test_archive_detail_state(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORING
        self.s3_file_object.available_until = datetime.now()
        self.assertEqual(S3ArchiveDetailState.RESTORED, self.s3_file_object.archive_detail_state)

    def test_storage_class_glacier_and_available_until(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        try:
            self.s3_file_object.available_until = datetime.now()
        except ValueError:
            self.fail("Unexpected ValueError")

    def test_storage_class_glacier_and_unarchived(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.NOT_ARCHIVED
        self.assertIsNone(self.s3_file_object.storage_class)

    def test_storage_class_standard_and_archived(self):
        self.s3_file_object.storage_class = S3StorageClass.STANDARD
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.ARCHIVED
        self.assertIsNone(self.s3_file_object.storage_class)

    def test_storage_class_standard_and_restoring(self):
        self.s3_file_object.storage_class = S3StorageClass.STANDARD
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORING
        self.assertIsNone(self.s3_file_object.storage_class)

    def test_storage_class_standard_test_archive_detail_state(self):
        self.s3_file_object.storage_class = S3StorageClass.STANDARD
        self.assertEqual(S3ArchiveDetailState.NOT_ARCHIVED, self.s3_file_object.archive_detail_state)

    def test_storage_class_standard_and_archive_storage_class(self):
        self.s3_file_object.storage_class = S3StorageClass.STANDARD
        self.assertFalse(self.s3_file_object.archive_storage_class)

    def test_transition_restored_to_archived(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORED
        self.s3_file_object.available_until = datetime.now()
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.ARCHIVED
        self.assertIsNone(self.s3_file_object.available_until)

    def test_transition_restored_to_standard_available_until(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORED
        self.s3_file_object.available_until = datetime.now()
        self.s3_file_object.storage_class = S3StorageClass.STANDARD
        self.assertIsNone(self.s3_file_object.available_until)

    def test_restored_and_set_available_until_to_None(self):
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORED
        try:
            self.s3_file_object.available_until = None
        except ValueError:
            self.fail("Unexpected ValueError")

    def test_restored_and_set_available_until_to_None_check_restored(self):
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORED
        self.s3_file_object.available_until = None
        self.assertEqual(S3ArchiveDetailState.RESTORED, self.s3_file_object.archive_detail_state)

    def test_restored_and_set_storage_class_to_None(self):
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORED
        try:
            self.s3_file_object.storage_class = None
        except ValueError:
            self.fail("Unexpected ValueError")

    def test_restored_and_set_storage_class_to_None_check_archive_detail_state(self):
        self.s3_file_object.archive_detail_state = S3ArchiveDetailState.RESTORED
        self.s3_file_object.storage_class = None
        self.assertEqual(S3ArchiveDetailState.RESTORED, self.s3_file_object.archive_detail_state)

    def test_glacier_and_set_archive_detail_state_to_None_check_glacier(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.s3_file_object.archive_detail_state = None
        self.assertEqual(S3StorageClass.GLACIER, self.s3_file_object.storage_class)

    def test_available_until_set_standard_check_available_until(self):
        self.s3_file_object.available_until = datetime.now()
        self.s3_file_object.storage_class = S3StorageClass.STANDARD
        self.assertIsNone(self.s3_file_object.available_until)

    def test_available_until_set_glacier_check_available_until(self):
        self.s3_file_object.available_until = datetime.now()
        self.s3_file_object.storage_class = S3StorageClass.GLACIER
        self.assertIsNotNone(self.s3_file_object.available_until)

    def test_none_storage_class(self):
        d = {'created': '2022-05-17T00:00:00+00:00', 'derived_by': None, 'derived_from': [], 'description': None,
             'display_name': 'TextFileUTF8.txt', 'id': 'VGV4dEZpbGVVVEY4LnR4dA==', 'invites': [],
             'modified': '2022-05-17T00:00:00+00:00', 'name': 'VGV4dEZpbGVVVEY4LnR4dA==', 'owner': 'system|none',
             'shares': [], 'source': 'AWS Simple Cloud Storage (S3)', 'storage_class': 'STANDARD',
             'type': 'heaobject.data.AWSS3FileObject',
             's3_uri': 's3://arp-scale-2-cloud-bucket-with-tags11/TextFileUTF8.txt', 'presigned_url': None,
             'version': None, 'versions': [], 'mime_type': 'text/plain', 'size': 1253915,
             'human_readable_size': '1.3 MB', 'bucket_id': 'arp-scale-2-cloud-bucket-with-tags11',
             'key': 'TextFileUTF8.txt', 'retrievable': True, 'human_readable_archive_detail_state': 'Not Archived',
             'archive_detail_state': 'NOT_ARCHIVED', 'archive_storage_class': False, 'available_until': None}
        self.s3_file_object.from_dict(d)
        self.assertEqual(S3StorageClass.STANDARD, self.s3_file_object.storage_class)

    def test_glacier_ir_archive_detail(self):
        self.s3_file_object.storage_class = S3StorageClass.GLACIER_IR
        self.assertEqual(S3ArchiveDetailState.ARCHIVED, self.s3_file_object.archive_detail_state)


def default_archive_detail_state_for_each_storage_class_tests():
    """Generates tests for the default archive_detail_state for each storage class. The test names have the format
    test_archive_detail_state_for_each_storage_class_<storage_class>:<archive_detail_state>, where <storage_class> is
    the name of the storage class being tested and <archive_detail_state> is the name of the
    expected archive_detail_state for the storage class. If the storage class requires a restore, the default
    archive_detail_state is None."""
    def storage_class_test_generator(storage_class: S3StorageClass, archive_detail_state: S3ArchiveDetailState):
        def test(self: TestAWSS3FileObject):
            self.s3_file_object.storage_class = storage_class
            self.assertEqual(archive_detail_state, self.s3_file_object.archive_detail_state)
        return test

    for storage_class in S3StorageClass:
        archive_detail_state = storage_class._default_archive_state
        test_name = f'test_archive_detail_state_for_each_storage_class_{storage_class.name}:{archive_detail_state.name if archive_detail_state else None}'
        test = storage_class_test_generator(storage_class, archive_detail_state)
        setattr(TestAWSS3FileObject, test_name, test)
default_archive_detail_state_for_each_storage_class_tests()


def storage_class_retrievable_tests():
    """Generates tests for whether the file object's retrievable attribute has the correct value after the object's
    storage class is set. The test names have the format test_storage_class_retrievable_<storage_class>, where
    <storage_class> is the name of the storage class being tested."""

    def storage_class_test_generator(storage_class: S3StorageClass):
        """storage_class_test_generators needs to be wrapped in a function to prevent pytest from attempting to run
        test and erroring out."""
        def test(self: TestAWSS3FileObject):
            """Test that storage classes that require a restore result in a file object that is either not retrievable
            or retrievability is undefined (None). Storage classes that do not require a restore should result in a
            file object that is retrievable."""
            self.s3_file_object.storage_class = storage_class
            self.assertEqual(self.s3_file_object.storage_class.requires_restore, not self.s3_file_object.retrievable)
        return test

    for storage_class in S3StorageClass:
        test_name = f'test_storage_class_retrievable_{storage_class.name}'
        test = storage_class_test_generator(storage_class)
        setattr(TestAWSS3FileObject, test_name, test)
storage_class_retrievable_tests()
