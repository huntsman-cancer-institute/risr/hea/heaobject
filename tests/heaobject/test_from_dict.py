from unittest import TestCase
from heaobject import data, registry, folder, root
import json


class TestFromJSONAndDict(TestCase):

    def test_from_dict(self):
        expected = data.DataFile()
        expected.name = 'foo'
        expected.description = 'bar'
        ds = data.DataFile()
        ds.from_dict({
            'type': data.DataFile.get_type_name(),
            'name': 'foo',
            'description': 'bar'
        })
        self.assertEqual(expected.to_dict(), ds.to_dict())

    def test_from_dict_with_nested(self):
        expected = registry.Component()
        expected.name = 'foo'
        expected.description = 'bar'
        resource = registry.Resource()
        resource.resource_type_name = folder.Folder.get_type_name()
        expected.resources = [resource]
        jsn_str = json.dumps({
            'type': registry.Component.get_type_name(),
            'name': 'foo',
            'description': 'bar',
            'resources': [
                {
                    'type': registry.Resource.get_type_name(),
                    'resource_type_name': folder.Folder.get_type_name()
                }
            ]
        })
        c = registry.Component()
        c.from_json(jsn_str)
        self.maxDiff = None
        self.assertDictEqual(expected.to_dict(), c.to_dict())

    def test_from_dict_with_read_only_property(self):
        expected = folder.Folder()
        expected.name = 'foo'
        expected.description = 'bar'
        f = folder.Folder()
        f.from_dict({
            'type': folder.Folder.get_type_name(),
            'name': 'foo',
            'description': 'bar',
            'mime_type': 'application/x.folder',
        })
        self.assertEqual(expected.to_dict(), f.to_dict())

    def test_from_dict_with_monkey_patching_attempt(self):
        expected = folder.Folder()
        expected.name = 'foo'
        expected.description = 'bar'
        f = folder.Folder()
        f.from_dict({
            'type': folder.Folder.get_type_name(),
            'name': 'foo',
            'description': 'bar',
            'my_monkey_patched_attribute': 'here it is',
        })
        self.assertEqual(expected.to_dict(), f.to_dict())

    def test_from_dict_module_level_bad_type(self):
        """Checks if passing in an invalid type into the HEAObject root from_dict function raises a TypeError."""
        self.assertRaises(TypeError, root.from_dict, {
            'type': 'foo.bar',
            'name': 'foobar',
            'description': 'Some spam'
        })

    def test_desktop_object_from_dict_module_level_bad_type(self):
        """Checks if passing in an HEAObject type into the root desktop_object_from_dict function raises a TypeError."""
        self.assertRaises(TypeError, root.desktop_object_from_dict, {
            'type': 'heaobject.root.MemberObject',
            'name': 'member',
            'description': 'A member of something'
        })

    def test_desktop_object_from_json_module_level(self):
        try:
            root.desktop_object_from_json('''{
                "type": "heaobject.volume.Volume",
                "name": "volume",
                "description": "A volume"
            }''')
        except Exception as e:
            self.fail(f'Got Exception {e} unexpectedly')

    def test_desktop_object_from_json_module_level_bad_type(self):
        self.assertRaises(TypeError, root.desktop_object_from_json, '''{
            "type": "heaobject.root.MemberObject",
            "name": "member",
            "description": "A member of something"
        }''')

    def test_desktop_object_from_json_module_level_missing_type(self):
        self.assertRaises(ValueError, root.desktop_object_from_json, '''{
               "name": "member",
               "description": "A member of something"
           }''')
