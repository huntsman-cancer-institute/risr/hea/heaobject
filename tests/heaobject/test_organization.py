from unittest import TestCase, IsolatedAsyncioTestCase
from heaobject.organization import Organization, permission_id_dict
from heaobject.user import NONE_USER, TEST_USER
from heaobject.root import PermissionContext, Permission, Share, ShareImpl


class TestOrganization(TestCase):
    def setUp(self) -> None:
        self.__test_org = Organization()

    def test_account_ids_initial_value(self):
        self.assertEqual([], self.__test_org.account_ids)

    def test_account_ids_set_none(self):
        self.__test_org.account_ids = None
        self.assertEqual([], self.__test_org.account_ids)

    def test_account_ids_set_list_str(self):
        self.__test_org.account_ids = ['123a', '456b', '789c']
        self.assertEqual(set(['123a', '456b', '789c']), set(self.__test_org.account_ids))

    def test_account_ids_set_list_arbitrary(self):
        self.__test_org.account_ids = [int, str, ('foo', 'bar')]
        self.assertEqual(set([str(int), str(str), str(('foo', 'bar'))]), set(self.__test_org.account_ids))

    def test_account_ids_add_str(self):
        self.__test_org.add_account_id('123a')
        self.assertEqual(['123a'], self.__test_org.account_ids)

    def test_account_ids_add_arbitrary(self):
        self.__test_org.add_account_id(int)  # type: ignore
        self.assertEqual([str(int)], self.__test_org.account_ids)

    def test_account_ids_remove(self):
        self.__test_org.account_ids = ['123a', '456b', '789c']
        self.__test_org.remove_account_id('456b')
        self.assertEqual(set(['123a', '789c']), set(self.__test_org.account_ids))

    def test_account_ids_remove_does_not_exist(self):
        self.__test_org.account_ids = ['123a', '456b', '789c']
        with self.assertRaises(ValueError):
            self.__test_org.remove_account_id('foo')

    def test_principal_investigator_id_initial_value(self):
        self.assertEqual(None, self.__test_org.principal_investigator_id)

    def test_principal_investigator_id_set_none(self):
        self.__test_org.principal_investigator_id = None
        self.assertEqual(None, self.__test_org.principal_investigator_id)

    def test_principal_investigator_id_set_str(self):
        self.__test_org.principal_investigator_id = 'c2hlcmxvy2tob2xtzxmu'
        self.assertEqual('c2hlcmxvy2tob2xtzxmu', self.__test_org.principal_investigator_id)

    def test_principal_investigator_id_set_arbitrary(self):
        self.__test_org.principal_investigator_id = int
        self.assertEqual(str(int), self.__test_org.principal_investigator_id)

    def test_manager_ids_initial_value(self):
        self.assertEqual([], self.__test_org.manager_ids)

    def test_manager_ids_set_none(self):
        self.__test_org.manager_ids = None
        self.assertEqual([], self.__test_org.manager_ids)

    def test_manager_ids_set_list_str(self):
        self.__test_org.manager_ids = ['123a', '456b', '789c']
        self.assertEqual(['123a', '456b', '789c'], self.__test_org.manager_ids)

    def test_manager_ids_set_list_arbitrary(self):
        self.__test_org.manager_ids = [int, str, ('foo', 'bar')]
        self.assertEqual([str(int), str(str), str(('foo', 'bar'))], self.__test_org.manager_ids)

    def test_manager_ids_add_str(self):
        self.__test_org.add_manager_id('123a')
        self.assertEqual(['123a'], self.__test_org.manager_ids)

    def test_manager_ids_add_arbitrary(self):
        self.__test_org.add_manager_id(int)  # type: ignore
        self.assertEqual([str(int)], self.__test_org.manager_ids)

    def test_manager_ids_remove(self):
        self.__test_org.manager_ids = ['123a', '456b', '789c']
        self.__test_org.remove_manager_id('456b')
        self.assertEqual(['123a', '789c'], self.__test_org.manager_ids)

    def test_manager_ids_remove_does_not_exist(self):
        self.__test_org.manager_ids = ['123a', '456b', '789c']
        with self.assertRaises(ValueError):
            self.__test_org.remove_manager_id('foo')

    def test_member_ids_initial_value(self):
        self.assertEqual([], self.__test_org.member_ids)

    def test_member_ids_set_none(self):
        self.__test_org.member_ids = None
        self.assertEqual([], self.__test_org.member_ids)

    def test_member_ids_set_list_str(self):
        self.__test_org.member_ids = ['123a', '456b', '789c']
        self.assertEqual(['123a', '456b', '789c'], self.__test_org.member_ids)

    def test_member_ids_set_list_arbitrary(self):
        self.__test_org.member_ids = [int, str, ('foo', 'bar')]
        self.assertEqual([str(int), str(str), str(('foo', 'bar'))], self.__test_org.member_ids)

    def test_member_ids_add_str(self):
        self.__test_org.add_member_id('123a')
        self.assertEqual(['123a'], self.__test_org.member_ids)

    def test_member_ids_add_arbitrary(self):
        self.__test_org.add_member_id(int)  # type: ignore
        self.assertEqual([str(int)], self.__test_org.member_ids)

    def test_member_ids_remove(self):
        self.__test_org.member_ids = ['123a', '456b', '789c']
        self.__test_org.remove_member_id('456b')
        self.assertEqual(['123a', '789c'], self.__test_org.member_ids)

    def test_member_ids_remove_does_not_exist(self):
        self.__test_org.member_ids = ['123a', '456b', '789c']
        with self.assertRaises(ValueError):
            self.__test_org.remove_member_id('foo')

    def test_dynamic_permissions_no_permissions(self):
        self.__test_org.add_member_id(TEST_USER)
        self.assertCountEqual([], self.__test_org.dynamic_permission(NONE_USER))

    def test_dynamic_permissions_member_permissions(self):
        self.__test_org.add_member_id(TEST_USER)
        self.assertCountEqual(permission_id_dict['member_ids'], self.__test_org.dynamic_permission(TEST_USER))

    def test_dynamic_permissions_manager_permissions(self):
        self.__test_org.add_manager_id(TEST_USER)
        self.assertCountEqual(permission_id_dict['manager_ids'], self.__test_org.dynamic_permission(TEST_USER))

    def test_list_attributes_passed_a_string(self):
        self.__test_org.manager_ids = TEST_USER
        self.assertEqual([TEST_USER], self.__test_org.manager_ids)

    def test_get_groups(self):
        self.__test_org.add_member_group_id('supreme leader')
        self.__test_org.add_member_id('reximus')
        self.assertEqual(['supreme leader'], self.__test_org.get_groups('reximus'))

class TestAsyncOrganizationCalls(IsolatedAsyncioTestCase):
    def setUp(self):
        self.__test_org = Organization()
        self.__context = PermissionContext(TEST_USER)
        share = ShareImpl()
        share.user = TEST_USER
        share.add_permission(Permission.CHECK_DYNAMIC)
        self.__test_org.add_share(share)

    async def test_dynamic_permissions(self):
        self.__test_org.add_admin_id(TEST_USER)
        expected_perms = [Permission.COOWNER]
        actual_perms = await self.__test_org.get_permissions(self.__context)
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_admin_can_change_admins(self):
        self.__test_org.add_admin_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('admin_ids', self.__context)
        expected_perms = [Permission.VIEWER, Permission.EDITOR]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_admin_can_change_pi(self):
        self.__test_org.add_admin_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('principal_investigator_id', self.__context)
        expected_perms = [Permission.VIEWER, Permission.EDITOR]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_admin_can_change_owner(self):
        self.__test_org.add_admin_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('owner', self.__context)
        expected_perms = [Permission.VIEWER, Permission.EDITOR]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_admin_can_change_accounts(self):
        self.__test_org.add_admin_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('account_ids', self.__context)
        expected_perms = [Permission.VIEWER, Permission.EDITOR]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_admin_can_change_admin_groups(self):
        self.__test_org.add_admin_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('admin_group_ids', self.__context)
        expected_perms = [Permission.VIEWER, Permission.EDITOR]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_manager_cannot_change_admins(self):
        self.__test_org.add_manager_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('admin_ids', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_manager_cannot_change_accounts(self):
        self.__test_org.add_manager_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('account_ids', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_manager_cannot_change_owner(self):
        self.__test_org.add_manager_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('owner', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_manager_cannot_change_pi(self):
        self.__test_org.add_manager_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('principal_investigator_id', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_manager_cannot_change_manager_groups(self):
        self.__test_org.add_manager_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('manager_group_ids', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_pi_can_change_pi(self):
        self.__test_org.principal_investigator_id = TEST_USER
        actual_perms = await self.__test_org.get_attribute_permissions('principal_investigator_id', self.__context)
        expected_perms = [Permission.VIEWER, Permission.EDITOR]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_pi_can_change_managers(self):
        self.__test_org.principal_investigator_id = TEST_USER
        actual_perms = await self.__test_org.get_attribute_permissions('manager_ids', self.__context)
        expected_perms = [Permission.VIEWER, Permission.EDITOR]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_pi_can_change_members(self):
        self.__test_org.principal_investigator_id = TEST_USER
        actual_perms = await self.__test_org.get_attribute_permissions('member_ids', self.__context)
        expected_perms = [Permission.VIEWER, Permission.EDITOR]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_pi_cannot_change_admins(self):
        self.__test_org.principal_investigator_id = TEST_USER
        actual_perms = await self.__test_org.get_attribute_permissions('admin_ids', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_pi_cannot_change_owner(self):
        self.__test_org.principal_investigator_id = TEST_USER
        actual_perms = await self.__test_org.get_attribute_permissions('owner', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_pi_cannot_change_member_groups(self):
        self.__test_org.principal_investigator_id = TEST_USER
        actual_perms = await self.__test_org.get_attribute_permissions('member_group_ids', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_member_cannot_change_admins(self):
        self.__test_org.add_member_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('admin_ids', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_member_cannot_change_accounts(self):
        self.__test_org.add_member_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('account_ids', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_member_cannot_change_owner(self):
        self.__test_org.add_member_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('owner', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_member_cannot_change_pi(self):
        self.__test_org.add_member_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('principal_investigator_id', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))

    async def test_dynamic_permissions_pi_cannot_change_member_groups(self):
        self.__test_org.add_member_id(TEST_USER)
        actual_perms = await self.__test_org.get_attribute_permissions('member_group_ids', self.__context)
        expected_perms = [Permission.VIEWER]
        self.assertEqual(set(expected_perms), set(actual_perms))
