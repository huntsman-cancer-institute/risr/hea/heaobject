from unittest import TestCase
from heaobject.root import ShareImpl, InviteImpl, Permission


class TestShareImpl(TestCase):
    def setUp(self) -> None:
        self.s = ShareImpl()

    def test_none_permissions(self) -> None:
        self.s.permissions = None
        self.assertEqual([], self.s.permissions)

    def test_list_of_strings_and_permissions(self) -> None:
        try:
            self.s.permissions = ['VIEWER', Permission.EDITOR]
        except Exception as e:
            self.fail(f'Did not correctly set permissions: {e}')

    def test_empty_list(self) -> None:
        self.s.permissions = []
        self.assertEqual([], self.s.permissions)

    def test_list_of_bad_types(self) -> None:
        with self.assertRaises(KeyError):
            self.s.permissions = ['EDITOR', None, Permission.VIEWER]

    def test_permission_list(self) -> None:
        self.s.permissions = ['EDITOR', Permission.VIEWER]
        self.assertEqual({Permission.EDITOR, Permission.VIEWER}, set(self.s.permissions))

    def test_int(self) -> None:
        with self.assertRaises(TypeError):
            self.s.permissions = 3


class TestInviteImpl(TestCase):
    def setUp(self) -> None:
        self.__invite = InviteImpl()

    def test_accepted_initial_value(self):
        self.assertIs(self.__invite.accepted, False)

    def test_accepted_set_none(self):
        self.__invite.accepted = None
        self.assertIs(self.__invite.accepted, False)

    def test_accepted_set_bool_true(self):
        self.__invite.accepted = True
        self.assertTrue(self.__invite.accepted)

    def test_accepted_set_yes(self):
        self.__invite.accepted = 'Yes'
        self.assertIs(self.__invite.accepted, True)

    def test_accepted_set_t(self):
        self.__invite.accepted = 'T'
        self.assertIs(self.__invite.accepted, True)

    def test_accepted_set_y(self):
        self.__invite.accepted = 'y'
        self.assertIs(self.__invite.accepted, True)

    def test_accepted_set_true_with_extraneous_spaces(self):
        self.__invite.accepted = '      tRuE   '
        self.assertIs(self.__invite.accepted, True)

    def test_accepted_set_arbitrary(self):
        self.__invite.accepted = 1
        self.assertIs(self.__invite.accepted, False)
