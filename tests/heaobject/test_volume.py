from unittest import TestCase
from heaobject import volume


class TestVolume(TestCase):
    def setUp(self) -> None:
        self.__test_volume = volume.Volume()

    def test_mime_type(self):
        self.assertEqual('application/x.volume', self.__test_volume.mime_type)

    def test_folder_id_initial_value(self):
        self.assertIsNone(self.__test_volume.folder_id)

    def test_folder_id_set_none(self):
        self.__test_volume.folder_id = None
        self.assertIsNone(self.__test_volume.folder_id)

    def test_folder_id_set_str(self):
        self.__test_volume.folder_id = '1234'
        self.assertEqual('1234', self.__test_volume.folder_id)

    def test_folder_id_set_int(self):
        self.__test_volume.folder_id = '5678'
        self.assertEqual('5678', self.__test_volume.folder_id)

    def test_file_system_name_initial_value(self):
        self.assertEqual(volume.DEFAULT_FILE_SYSTEM, self.__test_volume.file_system_name)

    def test_file_system_name_set_none(self):
        self.__test_volume.file_system_name = None
        self.assertEqual(volume.DEFAULT_FILE_SYSTEM, self.__test_volume.file_system_name)

    def test_file_system_name_set_str(self):
        try:
            self.__test_volume.file_system_name = 'DIFFERENT_FILE_SYSTEM'
            self.assertEqual('DIFFERENT_FILE_SYSTEM', self.__test_volume.file_system_name)
        except TypeError:
            self.fail(msg='Setting file_system_name unsuccessful with string')

    def test_file_system_name_set_arbitrary(self):
        try:
            self.__test_volume.file_system_name = 1234
            self.assertEqual('1234', self.__test_volume.file_system_name)
        except TypeError:
            self.fail(msg='Setting file_system_name unsuccessful with object that can be converted to a string')

    def test_file_system_type_initial_value(self):
        self.assertEqual(volume.MongoDBFileSystem.get_type_name(), self.__test_volume.file_system_type)

    def test_file_system_type_set_none(self):
        self.__test_volume.file_system_type = None
        self.assertEqual(volume.MongoDBFileSystem.get_type_name(), self.__test_volume.file_system_type)

    def test_file_system_type_set_mongodb(self):
        self.__test_volume.file_system_type = volume.MongoDBFileSystem.get_type_name()
        self.assertEqual(volume.MongoDBFileSystem.get_type_name(), self.__test_volume.file_system_type)

    def test_file_system_type_set_arbitrary(self):
        try:
            self.__test_volume.file_system_type = 1234
            self.assertEqual('1234', self.__test_volume.file_system_type)
        except TypeError:
            self.fail(msg='Setting file_system_type unsuccessful with object that can be converted to a string')

    def test_credential_id_initial_value(self):
        self.assertIsNone(self.__test_volume.credential_id)

    def test_credential_id_set_none(self):
        self.__test_volume.credential_id = None
        self.assertIsNone(self.__test_volume.credential_id)

    def test_credential_id_set_str(self):
        try:
            self.__test_volume.credential_id = 'Luximus'
            self.assertEqual('Luximus', self.__test_volume.credential_id)
        except TypeError:
            self.fail(msg='Setting credentials_id unsuccessful with a string')

    def test_credential_id_set_arbitrary(self):
        try:
            self.__test_volume.credential_id = 1234
            self.assertEqual('1234', self.__test_volume.credential_id)
        except TypeError:
            self.fail(msg='Setting credentials_id unsuccessful with object that can be converted to a string')

    def test_default_file_system_set_name(self):
        with self.assertRaises(AttributeError, msg="The DefaultFileSystem's name must always be DEFAULT_FILE_SYSTEM."):
            f = volume.DefaultFileSystem()
            f.name = 'DIFFERENT_FILE_SYSTEM'


class TestMongoDBFileSystem(TestCase):
    def setUp(self) -> None:
        self.__test_fs = volume.MongoDBFileSystem()

    def test_connection_string_initial_value(self):
        self.assertIsNone(self.__test_fs.connection_string)

    def test_connection_string_set_none(self):
        self.__test_fs.connection_string = None
        self.assertIsNone(self.__test_fs.connection_string)

    def test_connection_string_set_str(self):
        self.__test_fs.connection_string = 'mongodb://localhost:8080'
        self.assertEqual('mongodb://localhost:8080', self.__test_fs.connection_string)

    def test_connection_string_set_arbitrary(self):
        self.__test_fs.connection_string = int
        self.assertEqual(str(int), self.__test_fs.connection_string)

    def test_database_name_initial_value(self):
        self.assertIsNone(self.__test_fs.database_name)

    def test_database_name_set_none(self):
        self.__test_fs.database_name = None
        self.assertIsNone(self.__test_fs.database_name)

    def test_database_name_set_str(self):
        self.__test_fs.database_name = 'heaheahea'
        self.assertEqual('heaheahea', self.__test_fs.database_name)

    def test_database_name_set_arbitrary(self):
        self.__test_fs.database_name = int
        self.assertEqual(str(int), self.__test_fs.database_name)
