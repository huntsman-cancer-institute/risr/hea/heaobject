import unittest
from heaobject import aws
from itertools import chain



class TestS3URIMixinFunction(unittest.TestCase):
    pass


def s3_uri_function_tests():
    def my_test_generator(expected: str | None, bucket: str | None, key: str | None):
        def test(self):
            self.assertEqual(expected, aws.s3_uri(bucket, key))
        return test

    _test_data = [
        ('s3://my-bucket/', 'my-bucket', None),
        ('s3://my-bucket/my-folder/my-file', 'my-bucket', 'my-folder/my-file'),
        ('s3://my-bucket//', 'my-bucket', '/'),
        ('s3://my-bucket//hello/', 'my-bucket', '/hello/'),
        (None, '', 'hello'),
        (None, None, '/'),
        (None, None, None)
    ]
    for test_data in _test_data:
        _test_name = f'test_s3_uri_function_{test_data}'
        _test = my_test_generator(*test_data)
        setattr(TestS3URIMixinFunction, _test_name, _test)


class TestS3StorageClassMixin(unittest.TestCase):
    def setUp(self) -> None:
        class TestClass(aws.S3StorageClassMixin):
            pass

        self.t = TestClass()

    def test_invalid_storage_class_name(self):
        with self.assertRaises(ValueError):
            self.t.storage_class = 'MyFakeStorageClass'

    def test_none_storage_class_name(self):
        self.t.storage_class = None
        self.assertEqual(None, self.t.storage_class)


def storage_class_tests():
    def my_test_generator(a: str | aws.S3StorageClass):
        def test(self: TestS3StorageClassMixin):
            try:
                self.t.storage_class = a
            except AttributeError:
                self.fail(f'string "{a}" not accepted for storage_class property')

        return test

    for storage_class in chain(aws.S3StorageClass, [None]):
        _test_name = f'test_storage_class_strings_{storage_class}'
        _test = my_test_generator(storage_class.name if storage_class else None)
        setattr(TestS3StorageClassMixin, _test_name, _test)

    for storage_class in aws.S3StorageClass:
        _test_name = f'test_storage_class_enum_{storage_class}'
        _test = my_test_generator(storage_class)
        setattr(TestS3StorageClassMixin, _test_name, _test)


def storage_class_from_string_tests():
    def my_test_generator(a: str | aws.S3StorageClass):
        def test(self: TestS3StorageClassMixin):
            try:
                self.t.set_storage_class_from_str(a)
            except AttributeError:
                self.fail(f'string "{a}" not accepted for storage_class property')

        return test

    for storage_class in aws.S3StorageClass:
        _test_name = f'test_storage_class_enum_{storage_class.name}'
        _test = my_test_generator(storage_class.name)
        setattr(TestS3StorageClassMixin, _test_name, _test)


s3_uri_function_tests()
storage_class_tests()
storage_class_from_string_tests()
