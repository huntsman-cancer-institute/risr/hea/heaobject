from unittest import TestCase
from heaobject.person import Person, Role, EmailNotValidError, get_system_person, Group


class TestPerson(TestCase):
    def setUp(self) -> None:
        self.__test_person = Person()

    def test_preferred_name_initial_value(self):
        self.assertIsNone(self.__test_person.preferred_name)

    def test_preferred_name_set_none(self):
        self.__test_person.preferred_name = None
        self.assertIsNone(self.__test_person.preferred_name)

    def test_preferred_name_set_str(self):
        self.__test_person.preferred_name = "Henry"
        self.assertEqual(self.__test_person.preferred_name, "Henry")

    def test_preferred_name_set_arbitrary(self):
        self.__test_person.preferred_name = 1234
        self.assertEqual('1234', self.__test_person.preferred_name)

    def test_first_name_initial_value(self):
        self.assertIsNone(self.__test_person.first_name)

    def test_first_name_set_none(self):
        self.__test_person.first_name = None
        self.assertIsNone(self.__test_person.first_name)

    def test_first_name_set_str(self):
        self.__test_person.first_name = "Henry"
        self.assertEqual(self.__test_person.first_name, "Henry")

    def test_first_name_set_arbitrary(self):
        self.__test_person.first_name = 1234
        self.assertEqual('1234', self.__test_person.first_name)

    def test_last_name_initial_value(self):
        self.assertIsNone(self.__test_person.last_name)

    def test_last_name_set_none(self):
        self.__test_person.last_name = None
        self.assertIsNone(self.__test_person.last_name)

    def test_last_name_set_str(self):
        self.__test_person.last_name = "Baxter"
        self.assertEqual(self.__test_person.last_name, "Baxter")

    def test_last_name_set_arbitrary(self):
        self.__test_person.last_name = 1234
        self.assertEqual('1234', self.__test_person.last_name)

    def test_title_initial_value(self):
        self.assertIsNone(self.__test_person.title)

    def test_title_set_none(self):
        self.__test_person.title = None
        self.assertIsNone(self.__test_person.title)

    def test_title_set_str(self):
        self.__test_person.title = "Professor"
        self.assertEqual(self.__test_person.title, "Professor")

    def test_title_set_arbitrary(self):
        self.__test_person.title = 1234
        self.assertEqual('1234', self.__test_person.title)

    def test_email_initial_value(self):
        self.assertIsNone(self.__test_person.email)

    def test_email_set_none(self):
        self.__test_person.email = None
        self.assertIsNone(self.__test_person.email)

    def test_email_set_valid(self):
        self.__test_person.email = 'henry.baxter@hci.utah.edu'
        self.assertEqual(self.__test_person.email, 'henry.baxter@hci.utah.edu')

    def test_email_set_invalid(self):
        with self.assertRaises(EmailNotValidError):
            self.__test_person.email = 'henry.baxterhci.utah.edu'

    def test_email_set_not_str(self):
        with self.assertRaises(EmailNotValidError):
            self.__test_person.email = 123

    def test_phone_number_initial_value(self):
        self.assertIsNone(self.__test_person.phone_number)

    def test_phone_number_set_none(self):
        self.__test_person.phone_number = None
        self.assertIsNone(self.__test_person.phone_number)

    def test_phone_number_set_str(self):
        self.__test_person.phone_number = '123-456-7890'
        self.assertEqual(self.__test_person.phone_number, '123-456-7890')

    def test_phone_number_set_int(self):
        self.__test_person.phone_number = 1234567890
        self.assertEqual('1234567890', self.__test_person.phone_number)

    def test_display_name_neither_first_nor_last(self):
        self.assertEqual('Untitled Person', self.__test_person.display_name)

    def test_display_name_first_only(self):
        self.__test_person.first_name = 'Henry'
        self.assertEqual(self.__test_person.first_name, self.__test_person.display_name)

    def test_display_name_last_only(self):
        self.__test_person.last_name = 'Baxter'
        self.assertEqual(self.__test_person.last_name, self.__test_person.display_name)

    def test_display_name_first_and_last(self):
        self.__test_person.first_name = 'Henry'
        self.__test_person.last_name = 'Baxter'
        self.assertEqual(f'{self.__test_person.first_name} {self.__test_person.last_name}',
                         self.__test_person.display_name)

    def test_invalid_system_user(self):
        with self.assertRaises(ValueError):
            get_system_person('luximus')


class TestRole(TestCase):
    def setUp(self) -> None:
        self.__role = Role()

    def test_id_role(self):
        self.__role.id = 'a1c='
        self.assertEqual('kW', self.__role.role)

    def test_role_id(self):
        self.__role.role = 'kW'
        self.assertEqual('a1c=', self.__role.id)

    def test_name_role(self):
        self.__role.name = 'a1c='
        self.assertEqual('kW', self.__role.role)

    def test_role_name(self):
        self.__role.role = 'kW'
        self.assertEqual('a1c=', self.__role.name)

class TestGroup(TestCase):
    def setUp(self) -> None:
        self.__group = Group()

    def test_name_group(self):
        self.__group.name = 'a1c='
        self.assertEqual('kW', self.__group.group)

    def test_group_name(self):
        self.__group.group = 'kW'
        self.assertEqual('a1c=', self.__group.name)
