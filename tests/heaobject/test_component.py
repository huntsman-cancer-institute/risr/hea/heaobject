from unittest import TestCase
from heaobject.registry import Component
from heaobject.user import NONE_USER, ALL_USERS
from heaobject.root import Permission
from heaobject.volume import DEFAULT_FILE_SYSTEM
from heaobject.error import DeserializeException
from datetime import datetime


db_store = [{
        'id': '666f6f2d6261722d71757578',
        'created': datetime(2021, 12, 2, 17, 31, 15, 630000),
        'derived_by': None,
        'derived_from': ['foo', 'bar'],
        'description': None,
        'display_name': 'Reximus',
        'invites': [],
        'modified': datetime(2021, 12, 2, 17, 31, 15, 630000),
        'name': 'reximus',
        'owner': NONE_USER,
        'shares': [{
            'type': 'heaobject.root.ShareImpl',
            'invite': None,
            'user': ALL_USERS,
            'permissions': [Permission.COOWNER.name]
        }],
        'source': None,
        'type': 'heaobject.registry.Component',
        'version': None,
        'base_url': 'http://localhost',
        'resources': [{
            'type': 'heaobject.registry.Resource',
            'resource_type_name': 'heaobject.folder.Folder',
            'base_path': 'folders',
            'file_system_name': DEFAULT_FILE_SYSTEM,
            'file_system_type': 'heaobject.volume.DefaultFileSystem'
        },
            {
                'type': 'heaobject.registry.Resource',
                'resource_type_name': 'heaobject.folder.Item',
                'base_path': 'items',
                'file_system_name': DEFAULT_FILE_SYSTEM,
                'file_system_type': 'heaobject.volume.DefaultFileSystem'
            }
        ]
    },
    {
        'id': '0123456789ab0123456789ab',
        'created': datetime(2021, 12, 2, 17, 31, 15, 630000),
        'derived_by': None,
        'derived_from': ['oof', 'rab'],
        'description': None,
        'display_name': 'Luximus',
        'invites': [],
        'modified': datetime(2021, 12, 2, 17, 31, 15, 630000),
        'name': 'luximus',
        'owner': NONE_USER,
        'shares': [{
            'type': 'heaobject.root.ShareImpl',
            'invite': None,
            'user': ALL_USERS,
            'permissions': [Permission.EDITOR.name]
        }],
        'source': None,
        'type': 'heaobject.registry.Component',
        'version': None,
        'base_url': 'http://localhost/foo',
        'resources': [{
            'type': 'heaobject.registry.Resource',
            'resource_type_name': 'heaobject.folder.Folder',
            'base_path': 'folders',
            'file_system_name': DEFAULT_FILE_SYSTEM,
            'file_system_type': 'heaobject.volume.DefaultFileSystem'
        },
            {
                'type': 'heaobject.registry.Resource',
                'resource_type_name': 'heaobject.folder.Item',
                'base_path': 'items',
                'file_system_name': DEFAULT_FILE_SYSTEM,
                'file_system_type': 'heaobject.volume.DefaultFileSystem'
            }]
    },
    {
        'id': '0123456789ab0123456789ac',
        'created': datetime(2021, 12, 2, 17, 31, 15, 630000),
        'derived_by': None,
        'derived_from': ['oof', 'rab'],
        'description': None,
        'display_name': 'Luximus',
        'invites': [],
        'modified': datetime(2021, 12, 2, 17, 31, 15, 630000),
        'name': 'luximus',
        'owner': NONE_USER,
        'shares': [{
            'type': 'heaobject.root.ShareImpl',
            'invite': None,
            'user': ALL_USERS,
            'permissions': [Permission.EDITOR.name]
        }],
        'source': None,
        'type': 'heaobject.registry.Component',
        'version': None,
        'base_url': 'http://localhost/foo',
        'resources': [{
            'type': 'heaobject.registry.Resource',
            'resource_type_name': 'heaobject.folder.Folder',
            'base_path': '/folders',  # bad path
            'file_system_name': DEFAULT_FILE_SYSTEM,
            'file_system_type': 'heaobject.volume.DefaultFileSystem'
        },
            {
                'type': 'heaobject.registry.Resource',
                'resource_type_name': 'heaobject.folder.Item',
                'base_path': '/items',  # bad path
                'file_system_name': DEFAULT_FILE_SYSTEM,
                'file_system_type': 'heaobject.volume.DefaultFileSystem'
            }]
    }

]


class TestComponent(TestCase):
    def test_resource_1(self):
        c = Component()
        try:
            c.from_dict(db_store[0])
        except (TypeError, ValueError) as e:
            self.fail(e)

    def test_resource_2(self):
        c = Component()
        try:
            c.from_dict(db_store[1])
        except (TypeError, ValueError) as e:
            self.fail(e)

    def test_resource_omit_file_system_name(self):
        c = Component()
        c.from_dict(db_store[0])
        self.assertEqual(c.resources[0], c.get_resource('heaobject.folder.Folder'))

    def test_resource(self):
        c = Component()
        c.from_dict(db_store[0])
        self.assertEqual(c.resources[0], c.get_resource('heaobject.folder.Folder'))

    def test_resource_wrong_type(self):
        c = Component()
        c.from_dict(db_store[0])
        self.assertEqual(None, c.get_resource('heaobject.registry.Component'))

    def test_resource_invalid_type(self):
        c = Component()
        c.from_dict(db_store[0])
        with self.assertRaises(ValueError):
            self.assertEqual(None, c.get_resource('heaobject.registry.Components'))

    def test_resource_url_omit_file_system_name(self):
        c = Component()
        c.from_dict(db_store[0])
        self.assertEqual('http://localhost/folders', c.get_resource_url('heaobject.folder.Folder'))

    def test_resource_url(self):
        c = Component()
        c.from_dict(db_store[0])
        self.assertEqual('http://localhost/folders', c.get_resource_url('heaobject.folder.Folder'))

    def test_resource_url_wrong_type(self):
        c = Component()
        c.from_dict(db_store[0])
        self.assertEqual(None, c.get_resource_url('heaobject.registry.Component'))

    def test_resource__url_invalid_type(self):
        c = Component()
        c.from_dict(db_store[0])
        with self.assertRaises(ValueError):
            self.assertEqual(None, c.get_resource_url('heaobject.registry.Components', DEFAULT_FILE_SYSTEM))

    def test_bad_base_path(self):
        c = Component()
        with self.assertRaises(DeserializeException):
            c.from_dict(db_store[2])
