from unittest import TestCase
from heaobject.volume import Volume

class TestInstanceUnique_id(TestCase):
    def test_none(self):
        v = Volume()
        self.assertIsNone(None, v.instance_id)

    def test_is_not_none(self):
        v = Volume()
        v.id = 'foo'
        self.assertEqual(f'{v.type}^{v.id}', v.instance_id)
