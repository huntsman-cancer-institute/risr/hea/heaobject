from unittest import TestCase
from json import JSONDecodeError

from heaobject import bucket
from heaobject.root import Tag
from s3_bucket_uri_test_util import id_name_display_name_bucket_id_tests


class TestAWSBucket(TestCase):
    def setUp(self) -> None:
        self.bucket = bucket.AWSBucket()

    def test_arn_initial_value(self):
        self.assertIsNone(self.bucket.arn)

    def test_arn_set_none(self):
        self.bucket.arn = None
        self.assertIsNone(self.bucket.arn)

    def test_arn_set_str(self):
        self.bucket.arn = "1234"
        self.assertEqual(self.bucket.arn, "1234")

    def test_encrypted_initial_value(self):
        self.assertIsNone(self.bucket.encrypted)

    def test_encrypted_set_none(self):
        self.bucket.encrypted = None
        self.assertIsNone(self.bucket.encrypted)

    def test_encrypted_set_bool_true(self):
        self.bucket.encrypted = True
        self.assertTrue(self.bucket.encrypted)

    def test_encrypted_set_yes(self):
        self.bucket.encrypted = 'Yes'
        self.assertIs(self.bucket.encrypted, True)

    def test_encrypted_set_t(self):
        self.bucket.encrypted = 'T'
        self.assertIs(self.bucket.encrypted, True)

    def test_encrypted_set_y(self):
        self.bucket.encrypted = 'y'
        self.assertIs(self.bucket.encrypted, True)

    def test_encrypted_set_true_with_extraneous_spaces(self):
        self.bucket.encrypted = '      tRuE   '
        self.assertIs(self.bucket.encrypted, True)

    def test_encrypted_set_arbitrary(self):
        self.bucket.encrypted = 1
        self.assertIs(self.bucket.encrypted, False)

    def test_locked_initial_value(self):
        self.assertIsNone(self.bucket.locked)

    def test_locked_set_none(self):
        self.bucket.locked = None
        self.assertIsNone(self.bucket.locked)

    def test_locked_set_bool_true(self):
        self.bucket.locked = True
        self.assertTrue(self.bucket.locked)

    def test_locked_set_yes(self):
        self.bucket.locked = 'Yes'
        self.assertIs(self.bucket.locked, True)

    def test_locked_set_t(self):
        self.bucket.locked = 'T'
        self.assertIs(self.bucket.locked, True)

    def test_locked_set_y(self):
        self.bucket.locked = 'y'
        self.assertIs(self.bucket.locked, True)

    def test_locked_set_true_with_extraneous_spaces(self):
        self.bucket.locked = '      tRuE   '
        self.assertIs(self.bucket.locked, True)

    def test_locked_set_arbitrary(self):
        self.bucket.locked = 1
        self.assertIs(self.bucket.locked, False)

    def test_region_initial_value(self):
        self.assertIsNone(self.bucket.region)

    def test_region_set_none(self):
        self.bucket.region = None
        self.assertIsNone(self.bucket.region)

    def test_region_set_str(self):
        valid_region = 'us-east-1'
        try:
            self.bucket.region = valid_region
        except ValueError:
            self.fail(f'{valid_region} should be a valid region')

    def test_region_set_invalid(self):
        self.assertRaises(ValueError, bucket.AWSBucket.region.fset, self.bucket, 'us-east')

    def test_region_set_arbitrary_type(self):
        self.assertRaises(ValueError, bucket.AWSBucket.region.fset, self.bucket, int)

    def test_size_initial_value(self):
        self.assertIsNone(self.bucket.size)

    def test_size_set_none(self):
        self.bucket.size = None
        self.assertIsNone(self.bucket.size)

    def test_size_sets_to_float(self):
        self.bucket.size = 34234
        self.assertIsInstance(self.bucket.size, float)

    def test_size_set_int(self):
        self.bucket.size = 34234
        self.assertEqual(self.bucket.size, 34234)

    def test_size_set_float(self):
        self.bucket.size = 34234.5
        self.assertEqual(self.bucket.size, 34234.5)

    def test_size_set_str(self):
        self.bucket.size = "34234.5"
        self.assertEqual(self.bucket.size, 34234.5)

    def test_size_set_invalid(self):
        with self.assertRaises(ValueError,
                               msg='Setting size successful with object that cannot be converted to a float'):
            self.bucket.size = "dfk23kffk"

    def test_object_count_initial_value(self):
        self.assertIsNone(self.bucket.object_count)

    def test_object_count_set_none(self):
        self.bucket.object_count = None
        self.assertIsNone(self.bucket.object_count)

    def test_object_count_set_int(self):
        self.bucket.object_count = 34234
        self.assertEqual(self.bucket.object_count, 34234)

    def test_object_count_set_float(self):
        self.bucket.object_count = 34.234
        self.assertEqual(self.bucket.object_count, 34)

    def test_object_count_set_str(self):
        self.bucket.object_count = "34234"
        self.assertEqual(self.bucket.object_count, 34234)

    def test_object_count_set_invalid(self):
        with self.assertRaises(ValueError,
                               msg='Setting size successful with object that cannot be converted to a int'):
            self.bucket.object_count = "dfk23kffk"

    def test_tags_initial_value(self):
        self.assertEqual(self.bucket.tags, [])

    def test_tags_set_none(self):
        self.bucket.tags = None
        self.assertEqual([], self.bucket.tags)

    def test_tags_set_valid(self):
        try:
            self.bucket.tags = [Tag(), Tag()]
        except ValueError as e:
            self.fail(msg=f'Valid list of Tags not accepted: {e}')

    def test_tags_set_invalid_dict(self):
        with self.assertRaises(ValueError,
                               msg='A dict was accepted'):
            self.bucket.tags = [Tag(),
                                {'key': 'tree', 'value': 'k-tree'},
                                Tag()]

    def test_tags_set_invalid_not_iterable(self):
        with self.assertRaises(ValueError,
                               msg='Non-iterable tag collection accepted'):
            self.bucket.tags = 1

    def test_tags_set_equal_values(self):
        tag = Tag()
        tag.key = 'key'
        tag.value = 'value'
        self.bucket.tags = [tag]
        t = Tag()
        t.key = 'key'
        t.value = 'value'
        self.assertEqual(self.bucket.tags[0].value, t.value)

    def test_tags_set_not_same(self):
        tag = Tag()
        tag.key = "key"
        tag.value = "value"
        self.bucket.tags = [tag]
        t = Tag()
        t.value = "value"
        t.key = "tree"
        self.assertIsNot(self.bucket.tags[0], t)


id_name_display_name_bucket_id_tests(TestAWSBucket, 'bucket')
