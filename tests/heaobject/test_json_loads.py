from unittest import TestCase
from heaobject import data
import json


class TestJSONLoadsAndDict(TestCase):

    def test_json_loads(self):
        expected = data.DataFile()
        expected.name = 'foo'
        expected.description = 'bar'
        jsn = {
            'type': data.DataFile.__module__ + '.' + data.DataFile.__name__,
            'name': 'foo',
            'description': 'bar'
        }
        ds = data.DataFile()
        ds.from_json(json.dumps(jsn))
        self.assertEqual(expected.to_dict(), ds.to_dict())


