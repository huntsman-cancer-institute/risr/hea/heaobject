import unittest

from heaobject.awss3key import encode_key
from heaobject.data import AWSS3FileObject
from heaobject.folder import AWSS3Folder, AWSS3ItemInFolder, AWSS3SearchItemInFolder


class TestFile(unittest.TestCase):

    def setUp(self):
        self.test_cls = AWSS3FileObject()

    def tearDown(self) -> None:
        self.test_cls = None

    def test_s3_uri_no_bucket(self):

        self.test_cls.key = 'my-folder/my-file'
        self.assertEqual(None, self.test_cls.s3_uri)

    def test_s3_uri_no_bucket_no_key(self):
        self.assertEqual(None, self.test_cls.s3_uri)

    def test_s3_uri_mixin_function(self):
        self.test_cls.bucket_id = 'my-bucket'
        self.test_cls.key = 'my-folder/my-file'
        self.assertEqual('s3://my-bucket/my-folder/my-file', self.test_cls.s3_uri)

    def test_s3_uri_mixin_function_no_key(self):
        self.test_cls.bucket_id = 'my-bucket'
        self.assertEqual('s3://my-bucket/', self.test_cls.s3_uri)

    def test_s3_uri_mixin_function_slash_folder(self):
        self.test_cls.bucket_id = 'my-bucket'
        self.test_cls.key = '/'
        self.assertEqual('s3://my-bucket//', self.test_cls.s3_uri)

    def test_s3_uri_mixin_function_no_bucket(self):
        try:
            self.test_cls.key = 'my-folder/my-file'
        except ValueError as e:
            self.fail(f'Should not happen: {e}')

    def test_s3_uri_mixin_function_no_bucket_no_key(self):
        try:
            self.test_cls.bucket_id = None
            self.test_cls.key = None
        except ValueError as e:
            self.fail(f'Should not happen: {e}')


class TestAWSFolderFromDict(unittest.TestCase):
    def setUp(self) -> None:
        self.data = {'bucket_id': 'bucket', 'created': None, 'derived_by': None, 'derived_from': [], 'description': None,
                     'display_name': 'hello', 'id': 'aGVsbG8v', 'invites': [], 'key': 'hello/',
                     'mime_type': 'application/x.folder', 'modified': None, 'name': 'aGVsbG8v', 'owner': 'system|none',
                     'presigned_url': None, 's3_uri': None, 'shares': [], 'source': None, 'storage_class': 'STANDARD',
                     'type': 'heaobject.folder.AWSS3Folder', 'version': None, 'path': '/bucket/hello/'}
        self.folder = AWSS3Folder()
        self.folder.from_dict(self.data)

    def test_display_name(self):
        self.assertEqual('hello', self.folder.display_name)


class TestAWSFolder(unittest.TestCase):
    def setUp(self):
        self.folder = AWSS3Folder()

    def test_reset_display_name(self):
        self.folder.display_name = 'hello'
        self.folder.key = None
        self.folder.display_name = 'hello'
        self.assertEqual('hello/', self.folder.key)

    def test_invalid_key_not_a_folder(self):
        with self.assertRaises(ValueError):
            self.folder.key = 'hello'

    def test_display_name_change_preserves_rest_of_key(self):
        self.folder.key = 'hello/goodbye/'
        self.folder.display_name = 'farewell'
        self.assertEqual('hello/farewell/', self.folder.key)

    def test_display_name_sets_key(self):
        self.folder.display_name = 'foobar'
        self.assertEqual('foobar/', self.folder.key)


class TestAWSS3SearchItemInFolder(unittest.TestCase):
    def setUp(self):
        self.item = AWSS3SearchItemInFolder()

    def test_reset_display_name(self):
        self.item.display_name = None
        self.item.path = None
        self.item.display_name = 'hello'
        self.assertEqual('hello', self.item.key)

    def test_is_delete_marker(self):
        try:
            self.item.is_delete_marker = None
            self.assertIsNone(self.item.is_delete_marker)
            self.item.is_delete_marker = True
            self.assertTrue(self.item.is_delete_marker)
            self.item.is_delete_marker = False
            self.assertFalse(self.item.is_delete_marker)
        except ValueError as e:
            self.fail(f'Should not happen: {e}')


    def test_event_name(self):
        try:
            self.item.event_name = None
            self.item.event_name = "ObjectRemoved:DeleteMarkerCreated"
        except ValueError as e:
            self.fail(f'Should not happen: {e}')

    def test_bad_event_name(self):
        try:
            self.item.event_name = None
            self.item.event_name = "DeleteMarkerCreated"
        except ValueError as e:
            self.assertIsNone(self.item.event_name)


class TesteAWSFolderFileItem(unittest.TestCase):
    def setUp(self):
        self.item = AWSS3ItemInFolder()

    def test_reset_display_name(self):
        self.item.display_name = 'hello'
        self.item.s3_uri = None
        self.item.display_name = 'hello'
        self.assertEqual('hello', self.item.key)

    def test_display_name_folder_change_preserves_rest_of_key(self):
        self.item.key = 'hello/goodbye/'
        self.item.display_name = 'farewell'
        self.assertEqual('hello/farewell/', self.item.key)

    def test_display_name_file_change_preserves_rest_of_key(self):
        self.item.key = 'hello/goodbye'
        self.item.display_name = 'farewell'
        self.assertEqual('hello/farewell', self.item.key)

    def test_display_name_sets_key(self):
        self.item.display_name = 'foobar'
        self.assertEqual('foobar', self.item.key)
