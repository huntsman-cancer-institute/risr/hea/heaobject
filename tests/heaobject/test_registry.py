from unittest import TestCase
from heaobject.user import NONE_USER
from heaobject.registry import Component, Collection
from heaobject.folder import Folder
from heaobject.volume import DEFAULT_FILE_SYSTEM


class TestComponent(TestCase):
    def setUp(self) -> None:
        self.component = Component()
        self.component.from_dict({
            'id': '666f6f2d6261722d71757578',
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'description': None,
            'display_name': 'Reximus',
            'invites': [],
            'modified': None,
            'name': 'reximus',
            'owner': NONE_USER,
            'shares': [],
            'source': None,
            'type': 'heaobject.registry.Component',
            'version': None,
            'base_url': 'http://localhost/foo',
            'external_base_url': 'https://example.com/health/enterprise/analytics',
            'resources': [{
                'type': 'heaobject.registry.Resource',
                'resource_type_name': 'heaobject.folder.Folder',
                'base_path': 'folders',
                'file_system_name': 'DEFAULT_FILE_SYSTEM'
            }]
        })
        self.another_component = Component()
        self.another_component.from_dict({
            'id': '666f6f2d6261722d71757578',
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'description': None,
            'display_name': 'Reximus',
            'invites': [],
            'modified': None,
            'name': 'reximus',
            'owner': NONE_USER,
            'shares': [],
            'source': None,
            'type': 'heaobject.registry.Component',
            'version': None,
            'base_url': 'http://localhost/foo',
            'external_base_url': 'https://example.com/health/enterprise/analytics',
            'resources': [{
                'type': 'heaobject.registry.Resource',
                'resource_type_name': 'heaobject.folder.Folder',
                'base_path': 'folders',
                'file_system_name': 'DIFFERENT_FILE_SYSTEM'
            }]
        })
        self.component_with_no_file_system_name = Component()
        self.component_with_no_file_system_name.from_dict({
            'id': '666f6f2d6261722d71757578',
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'description': None,
            'display_name': 'Reximus',
            'invited': [],
            'modified': None,
            'name': 'reximus',
            'owner': NONE_USER,
            'shares': [],
            'source': None,
            'type': 'heaobject.registry.Component',
            'version': None,
            'base_url': 'http://localhost/foo',
            'external_base_url': 'https://example.com/health/enterprise/analytics',
            'resources': [{
                'type': 'heaobject.registry.Resource',
                'resource_type_name': 'heaobject.folder.Folder',
                'base_path': 'folders'
            }]
        })

    def tearDown(self) -> None:
        self.component = None
        self.another_component = None
        self.component_with_no_file_system_name = None

    def test_get_resource_url_by_type(self):
        self.assertEqual('http://localhost/foo/folders', self.component.get_resource_url(Folder.get_type_name()))

    def test_get_external_resource_url_by_type(self):
        self.assertEqual('https://example.com/health/enterprise/analytics/folders', self.component.get_external_resource_url(Folder.get_type_name()))

    def test_get_resource_url_by_type(self):
        self.assertEqual('http://localhost/foo/folders', self.component.get_resource_url(Folder.get_type_name()))

    def test_get_resource_url_none(self):
        self.assertIsNone(self.component.get_resource_url(Component.get_type_name()))

    def test_collection_relative_url(self):
        with self.assertRaises(ValueError):
            c = Collection()
            c.url = '/badurl'

    def test_collection_bad_url(self):
        with self.assertRaises(ValueError):
            c = Collection()
            c.url = 'http://localhost:port'
