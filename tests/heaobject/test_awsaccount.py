from unittest import TestCase
from heaobject.account import AWSAccount, EmailNotValidError


class TestAWSAccount(TestCase):

    def setUp(self) -> None:
        self.__test_awsaccount = AWSAccount()

    def test_account_id_initial_value(self):
        self.assertIsNone(self.__test_awsaccount.id)

    def test_account_id_set_none(self):
        self.__test_awsaccount.id = None
        self.assertIsNone(self.__test_awsaccount.id)

    def test_account_id_set_str(self):
        self.__test_awsaccount.id = '1234'
        self.assertEqual(self.__test_awsaccount.id, '1234')

    def test_account_id_set_int(self):
        self.__test_awsaccount.id = 1234
        self.assertEqual(self.__test_awsaccount.id, '1234')

    def test_account_name_initial_value(self):
        self.assertIsNone(self.__test_awsaccount.name)

    def test_account_name_set_none(self):
        self.__test_awsaccount.name = None
        self.assertIsNone(self.__test_awsaccount.name)

    def test_account_name_set_str(self):
        self.__test_awsaccount.name = 'HEA - 1'
        self.assertEqual(self.__test_awsaccount.name, 'HEA - 1')

    def test_account_name_set_arbitrary(self):
        self.__test_awsaccount.name = int
        self.assertEqual(self.__test_awsaccount.name, str(int))

    def test_full_name_initial_value(self):
        self.assertIsNone(self.__test_awsaccount.full_name)

    def test_full_name_set_none(self):
        self.__test_awsaccount.full_name = None
        self.assertIsNone(self.__test_awsaccount.full_name)

    def test_full_name_set_str(self):
        self.__test_awsaccount.full_name = 'Richard'
        self.assertEqual(self.__test_awsaccount.full_name, 'Richard')

    def test_full_name_set_arbitrary(self):
        self.__test_awsaccount.full_name = int
        self.assertEqual(self.__test_awsaccount.full_name, str(int))

    def test_phone_number_initial_value(self):
        self.assertIsNone(self.__test_awsaccount.phone_number)

    def test_phone_number_set_none(self):
        self.__test_awsaccount.phone_number = None
        self.assertIsNone(self.__test_awsaccount.phone_number)

    def test_phone_number_set_str(self):
        self.__test_awsaccount.phone_number = '123-456-7890'
        self.assertEqual(self.__test_awsaccount.phone_number, '123-456-7890')

    def test_phone_number_set_int(self):
        self.__test_awsaccount.phone_number = 1234567890
        self.assertEqual('1234567890', self.__test_awsaccount.phone_number)

    def test_alternate_contact_name_initial_value(self):
        self.assertIsNone(self.__test_awsaccount.alternate_contact_name)

    def test_alternate_contact_name_set_none(self):
        self.__test_awsaccount.alternate_contact_name = None
        self.assertIsNone(self.__test_awsaccount.alternate_contact_name)

    def test_alternate_contact_name_set_str(self):
        self.__test_awsaccount.alternate_contact_name = 'Bob'
        self.assertEqual(self.__test_awsaccount.alternate_contact_name, 'Bob')

    def test_alternate_contact_name_set_arbitrary(self):
        self.__test_awsaccount.alternate_contact_name = int
        self.assertEqual(self.__test_awsaccount.alternate_contact_name, str(int))

    def test_email_address_initial_value(self):
        self.assertIsNone(self.__test_awsaccount.email_address)

    def test_email_address_set_none(self):
        self.__test_awsaccount.email_address = None
        self.assertIsNone(self.__test_awsaccount.email_address)

    def test_email_address_set_valid(self):
        self.__test_awsaccount.email_address = 'test@hci.utah.edu'
        self.assertEqual(self.__test_awsaccount.email_address, 'test@hci.utah.edu')

    def test_email_address_set_invalid(self):
        with self.assertRaises(EmailNotValidError):
            self.__test_awsaccount.email_address = 'testhci.utah.edu'

    def test_email_address_set_not_str(self):
        with self.assertRaises(EmailNotValidError):
            self.__test_awsaccount.email_address = 123

    def test_alternate_email_address_initial_value(self):
        self.assertIsNone(self.__test_awsaccount.alternate_email_address)

    def test_alternate_email_address_set_none(self):
        self.__test_awsaccount.alternate_email_address = None
        self.assertIsNone(self.__test_awsaccount.alternate_email_address)

    def test_alternate_email_address_set_valid(self):
        self.__test_awsaccount.alternate_email_address = 'test@hci.utah.edu'
        self.assertEqual(self.__test_awsaccount.alternate_email_address, 'test@hci.utah.edu')

    def test_alternate_email_address_set_invalid(self):
        with self.assertRaises(EmailNotValidError):
            self.__test_awsaccount.alternate_email_address = 'testhci.utah.edu'

    def test_alternate_email_address_set_not_str(self):
        with self.assertRaises(EmailNotValidError):
            self.__test_awsaccount.alternate_email_address = 123

    def test_alternate_phone_number_initial_value(self):
        self.assertIsNone(self.__test_awsaccount.alternate_phone_number)

    def test_alternate_phone_number_set_none(self):
        self.__test_awsaccount.alternate_phone_number = None
        self.assertIsNone(self.__test_awsaccount.alternate_phone_number)

    def test_alternate_phone_number_set_str(self):
        self.__test_awsaccount.alternate_phone_number = '111-222-3333'
        self.assertEqual(self.__test_awsaccount.alternate_phone_number, '111-222-3333')

    def test_alternate_phone_number_set_int(self):
        self.__test_awsaccount.alternate_phone_number = 1234567890
        self.assertEqual('1234567890', self.__test_awsaccount.alternate_phone_number)
