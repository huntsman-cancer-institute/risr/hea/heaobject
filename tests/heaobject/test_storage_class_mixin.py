import unittest
from heaobject.aws import S3StorageClassMixin, S3StorageClass


class WithStorageClass(S3StorageClassMixin):
    pass


class StorageClassMixinCase(unittest.TestCase):

    def setUp(self) -> None:
        self.with_storage_class = WithStorageClass()

    def tearDown(self) -> None:
        self.with_storage_class = None

    def test_glacier(self):
        self.with_storage_class.storage_class = S3StorageClass.GLACIER
        self.assertEqual(S3StorageClass.GLACIER, self.with_storage_class.storage_class)

    def test_null(self):
        self.with_storage_class.storage_class = None
        self.assertEqual(None, self.with_storage_class.storage_class)

    def test_str(self):
        self.with_storage_class.storage_class = S3StorageClass.GLACIER.name
        self.assertEqual(S3StorageClass.GLACIER, self.with_storage_class.storage_class)

    def test_invalid_obj(self):
        self.assertRaises(ValueError, WithStorageClass.storage_class.fset, self.with_storage_class, 2)

    def test_invalid_str(self):
        self.assertRaises(ValueError, WithStorageClass.storage_class.fset, self.with_storage_class, 'foo')

    def test_invalid_str_special_method(self):
        self.assertRaises(ValueError, self.with_storage_class.set_storage_class_from_str, 'foo')

    def test_str_special_method(self):
        self.with_storage_class.set_storage_class_from_str(S3StorageClass.GLACIER.name)
        self.assertEqual(S3StorageClass.GLACIER, self.with_storage_class.storage_class)

    def test_null_special_method(self):
        self.with_storage_class.set_storage_class_from_str(None)
        self.assertEqual(None, self.with_storage_class.storage_class)

