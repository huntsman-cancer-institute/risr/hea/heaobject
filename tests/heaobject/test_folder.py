import unittest
from heaobject.folder import AWSS3Folder, Folder, Item
from heaobject.data import DataFile


class TestS3Folder(unittest.TestCase):

    def setUp(self):
        self.folder = AWSS3Folder()

    def test_valid_path(self):
        try:
            self.folder.path = '/hello/world/'
        except ValueError:
            self.fail()

    def test_missing_leading_slash(self):
        with self.assertRaises(ValueError):
            self.folder.path = 'hello/world/'

    def test_missing_trailing_slash(self):
        with self.assertRaises(ValueError):
            self.folder.path = '/hello/world'

    def test_relationship_with_key(self):
        self.folder.path = '/hello/world/'
        self.assertEqual('world/', self.folder.key)

    def test_relationship_with_path(self):
        self.folder.key = 'world/'
        self.folder.bucket_id = 'hello'
        self.assertEqual('/hello/world/', self.folder.path)

    def test_relationship_with_path_no_bucket_id(self):
        self.folder.key = 'world/'
        self.assertEqual(None, self.folder.path)

    def test_weird_key_for_folder_with_no_name(self):
        self.folder.key = '/'
        self.folder.bucket_id = 'bucket'
        self.assertEqual('/bucket//', self.folder.path)

    def test_key_for_path_for_folder_with_no_name(self):
        self.folder.path = '/bucket//'
        self.assertEqual('/', self.folder.key)


class TestFolder(unittest.TestCase):

    def setUp(self):
        self.folder = Folder()

    def test_valid_path(self):
        try:
            self.folder.path = '/hello/world/'
        except ValueError:
            self.fail()

    def test_invalid_path_missing_leading_slash(self):
        with self.assertRaises(ValueError):
            self.folder.path = 'hello/world/'

    def test_invalid_path_missing_trailing_slash(self):
        with self.assertRaises(ValueError):
            self.folder.path = '/hello/world'


class TestItem(unittest.TestCase):
    def setUp(self) -> None:
        self.item = Item()

    def test_valid_path(self):
        self.item.actual_object_type_name = DataFile.get_type_name()
        try:
            self.item.path = '/hello/world'
        except ValueError:
            self.fail()

    def test_path_with_trailing_slash(self):
        self.item.actual_object_type_name = DataFile.get_type_name()
        with self.assertRaises(ValueError):
            self.item.path = '/hello/world/'
