from unittest import TestCase
from heaobject.folder import Item


class TestFolderItem(TestCase):
    def test_actual_object_type_name_initial_value(self):
        self.assertIsNone(Item().actual_object_type_name)

    def test_actual_object_type_name_set_none(self):
        item = Item()
        item.actual_object_type_name = None
        self.assertIsNone(item.actual_object_type_name)

    def test_actual_object_type_name_set_str(self):
        item = Item()
        item.actual_object_type_name = 'heaobject.registry.Component'
        self.assertEqual('heaobject.registry.Component', item.actual_object_type_name)

    def test_actual_object_type_name_set_arbitrary_type(self):
        item = Item()
        item.actual_object_type_name = int
        self.assertEqual(str(int), item.actual_object_type_name)

    def test_actual_object_id_initial_value(self):
        self.assertIsNone(Item().actual_object_id)

    def test_actual_object_id_set_none(self):
        item = Item()
        item.actual_object_id = None
        self.assertIsNone(item.actual_object_id)

    def test_actual_object_id_set_str(self):
        item = Item()
        item.actual_object_id = 'ahvudhntyw5jyw5jzxjpbnn0axr1dguh'
        self.assertEqual('ahvudhntyw5jyw5jzxjpbnn0axr1dguh', item.actual_object_id)

    def test_actual_object_id_set_arbitrary_type(self):
        item = Item()
        item.actual_object_id = int
        self.assertEqual(str(int), item.actual_object_id)

    def test_actual_object_uri_initial_value(self):
        self.assertIsNone(Item().actual_object_uri)

    def test_actual_object_uri_set_none(self):
        item = Item()
        item.actual_object_uri = None
        self.assertIsNone(item.actual_object_uri)

    def test_actual_object_uri_set_str(self):
        item = Item()
        item.actual_object_uri = 'https://localhost:8080/components/ahvudhntyw5jyw5jzxjpbnn0axr1dguh'
        self.assertEqual('https://localhost:8080/components/ahvudhntyw5jyw5jzxjpbnn0axr1dguh', item.actual_object_uri)

    def test_actual_object_uri_set_arbitrary_type(self):
        item = Item()
        item.actual_object_uri = int
        self.assertEqual(str(int), item.actual_object_uri)

    def test_folder_id_initial_value(self):
        self.assertIsNone(Item().folder_id)

    def test_folder_id_set_none(self):
        item = Item()
        item.folder_id = None
        self.assertIsNone(item.folder_id)

    def test_folder_id_set_str(self):
        item = Item()
        item.folder_id = 'ahvudhntyw5jyw5jzxjpbnn0axr1dguh'
        self.assertEqual('ahvudhntyw5jyw5jzxjpbnn0axr1dguh', item.folder_id)

    def test_folder_id_set_arbitrary_type(self):
        item = Item()
        item.folder_id = int
        self.assertEqual(str(int), item.folder_id)

    def test_volume_id_initial_value(self):
        self.assertIsNone(Item().volume_id)

    def test_volume_id_set_none(self):
        item = Item()
        item.volume_id = None
        self.assertIsNone(item.volume_id)

    def test_volume_id_set_str(self):
        item = Item()
        item.volume_id = 'ahvudhntyw5jyw5jzxjpbnn0axr1dguh'
        self.assertEqual('ahvudhntyw5jyw5jzxjpbnn0axr1dguh', item.volume_id)

    def test_volume_id_set_arbitrary_type(self):
        item = Item()
        item.volume_id = int
        self.assertEqual(str(int), item.volume_id)

    def test_size_initial_value(self):
        self.assertIsNone(Item().size)

    def test_size_set_none(self):
        item = Item()
        item.size = None
        self.assertIsNone(item.size)

    def test_size_set_int(self):
        item = Item()
        item.size = 1 << 16
        self.assertEqual(1 << 16, item.size)

    def test_size_set_supports_int(self):
        item = Item()
        item.size = '123456789'
        self.assertEqual(123456789, item.size)

    def test_size_does_not_support_int(self):
        item = Item()
        with self.assertRaises(ValueError):
            item.size = 'foo'

    def test_natural_size(self):
        item = Item()
        item.size = 1 << 25
        self.assertEqual('33.6 MB', item.human_readable_size)
