from datetime import datetime, timezone, timedelta
from unittest import TestCase
from heaobject.keychain import Credentials, AWSCredentials, CredentialsLifespanClass


class TestCredentials(TestCase):
    def setUp(self) -> None:
        self.__test_creds = Credentials()

    def test_account_username_initial_value(self):
        self.assertIsNone(self.__test_creds.account)

    def test_account_username_set_none(self):
        self.__test_creds.account = None
        self.assertIsNone(self.__test_creds.account)

    def test_account_username_set_str(self):
        self.__test_creds.account = 'luximus'
        self.assertEqual('luximus', self.__test_creds.account)

    def test_account_username_set_arbitrary(self):
        self.__test_creds.account = int
        self.assertEqual(str(int), self.__test_creds.account)

    def test_where_initial_value(self):
        self.assertIsNone(self.__test_creds.where)

    def test_where_set_none(self):
        self.__test_creds.where = None
        self.assertIsNone(self.__test_creds.where)

    def test_where_set_str(self):
        self.__test_creds.where = 'https://localhost:8080/volumes/hea/awsaccounts/me'
        self.assertEqual('https://localhost:8080/volumes/hea/awsaccounts/me', self.__test_creds.where)

    def test_where_set_arbitrary(self):
        self.__test_creds.where = int
        self.assertEqual(str(int), self.__test_creds.where)

    def test_password_initial_value(self):
        self.assertIsNone(self.__test_creds.password)

    def test_password_set_none(self):
        self.__test_creds.password = None
        self.assertIsNone(self.__test_creds.password)

    def test_password_set_str(self):
        self.__test_creds.password = 'password123'
        self.assertEqual('password123', self.__test_creds.password)

    def test_password_set_arbitrary(self):
        self.__test_creds.password = int
        self.assertEqual(str(int), self.__test_creds.password)

    def test_default_change_expiration_None(self):
        self.__test_creds.expiration = datetime.now() + timedelta(hours=12)
        try:
            self.__test_creds.expiration = None
        except Exception as e:
            self.fail(f'Setting expiration to None should not raise an exception but raised: {e}')

    def test_short_lived_change_expiration_None(self):
        self.__test_creds.lifespan_class = CredentialsLifespanClass.SHORT_LIVED
        self.__test_creds.expiration = datetime.now() + timedelta(hours=12)
        try:
            self.__test_creds.expiration = None
        except Exception as e:
            self.fail(f'Setting expiration to None should not raise an exception but raised: {e}')

    def test_long_lived_change_expiration_None(self):
        self.__test_creds.lifespan_class = CredentialsLifespanClass.LONG_LIVED
        self.__test_creds.expiration = datetime.now() + timedelta(hours=12)
        try:
            self.__test_creds.expiration = None
        except Exception as e:
            self.fail(f'Setting expiration to None should not raise an exception but raised: {e}')

    def test_short_lived_set_expiration(self):
        self.__test_creds.lifespan_class = CredentialsLifespanClass.SHORT_LIVED
        try:
            self.__test_creds.expiration = datetime.now() + timedelta(hours=120)
        except Exception as e:
            self.fail(f'Setting expiration to None should not raise an exception but raised: {e}')

    def test_short_lived_raise_if_expiration_invalid(self):
        try:
            self.__test_creds.raise_if_expiration_invalid(datetime.now() + timedelta(hours=120), lifespan_class=CredentialsLifespanClass.SHORT_LIVED)
        except ValueError as e:
            self.fail(f'Expiration should be valid but raised: {e}')

    def test_long_lived_raise_if_expiration_invalid(self):
        try:
            self.__test_creds.raise_if_expiration_invalid(datetime.now() + timedelta(hours=120), lifespan_class=CredentialsLifespanClass.LONG_LIVED)
        except ValueError as e:
            self.fail(f'Expiration should be valid but raised: {e}')

    def test_long_lived_unspecified_expiration_raise_if_expiration_invalid(self):
        try:
            self.__test_creds.raise_if_expiration_invalid(lifespan=CredentialsLifespanClass.LONG_LIVED)
        except ValueError as e:
            self.fail(f'Expiration should be valid but raised: {e}')

    def test_default_raise_if_expiration_invalid(self):
        try:
            self.__test_creds.raise_if_expiration_invalid()
        except ValueError as e:
            self.fail(f'Expiration should be valid but raised: {e}')

    def test_has_expired_default(self):
        self.assertFalse(self.__test_creds.has_expired(1))

    def test_has_expired_None(self):
        self.__test_creds.expiration = None
        self.assertFalse(self.__test_creds.has_expired(1))

    def test_has_not_expired_yet_False(self):
        self.__test_creds.expiration = datetime.now() + timedelta(minutes=3)
        self.assertFalse(self.__test_creds.has_expired(2))

    def test_has_not_expired_yet_True(self):
        self.__test_creds.expiration = datetime.now() + timedelta(minutes=1)
        self.assertTrue(self.__test_creds.has_expired(2))

    def test_has_not_expired_yet_True_2(self):
        self.__test_creds.expiration = datetime.now() + timedelta(minutes=1)
        self.assertTrue(self.__test_creds.has_expired(2))

    def test_has_expired_same(self):
        self.__test_creds.expiration = datetime.now() + timedelta(minutes=1)
        self.assertTrue(self.__test_creds.has_expired(1))

    def test_has_expired_in_past(self):
        self.__test_creds.expiration = datetime.now() - timedelta(minutes=3)
        self.assertTrue(self.__test_creds.has_expired(2))


class TestAWSCredentials(TestCase):

    def setUp(self) -> None:
        self.__test_aws_creds = AWSCredentials()

    def test_temporary_link_to_lifespan_default(self):
        self.assertFalse(self.__test_aws_creds.temporary)

    def test_temporary_link_to_lifespan_true(self):
        self.__test_aws_creds.lifespan_class = CredentialsLifespanClass.SHORT_LIVED
        self.assertTrue(self.__test_aws_creds.temporary)

    def test_temporary_link_to_lifespan_false(self):
        self.__test_aws_creds.lifespan_class = CredentialsLifespanClass.LONG_LIVED
        self.assertFalse(self.__test_aws_creds.temporary)

    def test_lifespan_link_to_temporary_default(self):
        self.assertEqual(CredentialsLifespanClass.LONG_LIVED, self.__test_aws_creds.lifespan_class)

    def test_lifespan_link_to_temporary_true(self):
        self.__test_aws_creds.temporary = True
        self.assertEqual(CredentialsLifespanClass.SHORT_LIVED, self.__test_aws_creds.lifespan_class)

    def test_lifespan_link_to_temporary_false(self):
        self.__test_aws_creds.temporary = False
        self.assertEqual(CredentialsLifespanClass.LONG_LIVED, self.__test_aws_creds.lifespan_class)

    def test_lifespan_str_setter(self):
        self.__test_aws_creds.set_lifespan_class_from_str(CredentialsLifespanClass.SHORT_LIVED.name)
        self.assertEqual(CredentialsLifespanClass.SHORT_LIVED, self.__test_aws_creds.lifespan_class)

    def test_temporary_and_managed(self):
        self.__test_aws_creds.temporary = True
        self.__test_aws_creds.managed = True
        self.assertFalse(self.__test_aws_creds.temporary)
        self.assertTrue(self.__test_aws_creds.managed)

    def test_managed_and_temporary(self):
        self.__test_aws_creds.managed = True
        self.__test_aws_creds.temporary = True
        self.assertTrue(self.__test_aws_creds.temporary)
        self.assertFalse(self.__test_aws_creds.managed)

    def test_expiration_switching_to_temporary(self):
        self.__test_aws_creds.managed = True
        self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=70)
        with self.assertRaises(ValueError):
            self.__test_aws_creds.temporary = True

    def test_expiration_temporary(self):
        self.__test_aws_creds.temporary = True
        with self.assertRaises(ValueError):
            self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=13)

    def test_expiration_managed(self):
        self.__test_aws_creds.managed = True
        try:
            self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=73)
        except:
            self.fail('Expiration should be valid')

    def test_expiration_default_changed_lifespan(self):
        self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=13)
        with self.assertRaises(ValueError):
            self.__test_aws_creds.lifespan_class = CredentialsLifespanClass.SHORT_LIVED

    def test_expiration_managed_changed_lifespan(self):
        self.__test_aws_creds.managed = True
        self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=13)
        with self.assertRaises(ValueError):
            self.__test_aws_creds.lifespan_class = CredentialsLifespanClass.SHORT_LIVED

    def test_change_expiration_temporary(self):
        self.__test_aws_creds.temporary = True
        self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=12)
        with self.assertRaises(ValueError):
            self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=13)

    def test_temporary_change_expiration_None(self):
        self.__test_aws_creds.temporary = True
        self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=12)
        try:
            self.__test_aws_creds.expiration = None
        except Exception as e:
            self.fail(f'Setting expiration to None should not raise an exception but raised: {e}')

    def test_managed_change_expiration_None(self):
        self.__test_aws_creds.managed = True
        self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=12)
        try:
            self.__test_aws_creds.expiration = None
        except Exception as e:
            self.fail(f'Setting expiration to None should not raise an exception but raised: {e}')

    def test_default_change_expiration_None(self):
        self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=12)
        try:
            self.__test_aws_creds.expiration = None
        except Exception as e:
            self.fail(f'Setting expiration to None should not raise an exception but raised: {e}')

    def test_short_lived_change_expiration_None(self):
        self.__test_aws_creds.lifespan_class = CredentialsLifespanClass.SHORT_LIVED
        self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=12)
        try:
            self.__test_aws_creds.expiration = None
        except Exception as e:
            self.fail(f'Setting expiration to None should not raise an exception but raised: {e}')

    def test_long_lived_change_expiration_None(self):
        self.__test_aws_creds.lifespan_class = CredentialsLifespanClass.LONG_LIVED
        self.__test_aws_creds.expiration = datetime.now() + timedelta(hours=12)
        try:
            self.__test_aws_creds.expiration = None
        except Exception as e:
            self.fail(f'Setting expiration to None should not raise an exception but raised: {e}')

    def test_has_expired_None(self):
        self.__test_aws_creds.expiration = None
        self.assertFalse(self.__test_aws_creds.has_expired(1))

    def test_has_expired_None_temporary(self):
        self.__test_aws_creds.temporary = True
        self.__test_aws_creds.expiration = None
        self.assertTrue(self.__test_aws_creds.has_expired(1))

    def test_has_expired_True_string(self):
        self.__test_aws_creds.expiration = "2022-10-26T12:34:41Z"
        self.assertTrue(self.__test_aws_creds.has_expired())

    def test_has_expired_False_string(self):
        self.__test_aws_creds.expiration = datetime.strftime(datetime.now(timezone.utc) + timedelta(hours=1), "%Y-%m-%dT%H:%M:%S%z" )
        self.assertFalse(self.__test_aws_creds.has_expired())

    def test_has_expired_invalid_datetime_format(self):
        with self.assertRaises(Exception, msg='Invalid datetime format'):
            self.__test_aws_creds.expiration = "2022-10T12:34:41Z"
            self.__test_aws_creds.has_expired()
