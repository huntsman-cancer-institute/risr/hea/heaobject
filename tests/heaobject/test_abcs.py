import heaobject
from heaobject.root import HEAObject
from heaobject.data import SameMimeType

import importlib
import pkgutil
from abc import ABC, ABCMeta
from unittest import TestCase

# Every heaobject module needs to be imported for __subclasses__ to give a complete list
for module in pkgutil.iter_modules(heaobject.__path__, prefix='heaobject.'):
    importlib.import_module(module.name)


def _get_all_abstract_subclasses(superclass: type) -> list[ABCMeta]:
    result = list({cls for cls in superclass.__subclasses__() if ABC in cls.__bases__})
    for cls in result:
        result.extend(_get_all_abstract_subclasses(cls))
    return result


def _get_test_case() -> type[TestCase]:
    def get_test(cls: ABCMeta):
        def _test_init_abstract_class(self: TestCase) -> None:
            with self.assertRaises(TypeError, msg=f'Initialized abstract class {cls.__module__}.{cls.__name__}'):
                cls()

        return _test_init_abstract_class

    tests = {}
    for abc in {HEAObject, SameMimeType} | set(_get_all_abstract_subclasses(HEAObject)) | \
               set(_get_all_abstract_subclasses(SameMimeType)):
        tests[f'test_init_abc_{abc.__name__.lower()}'] = get_test(abc)

    return type('CustomTestCase', (TestCase,), tests)  # type: ignore


class TestHEAObjectAbstractClasses(_get_test_case()):
    pass
