from unittest import TestCase

from heaobject import data, registry, folder, user, keychain
from heaobject.volume import MongoDBFileSystem


class TestToJSONAndDict(TestCase):

    def setUp(self):
        self.maxDiff = None

    def test_to_dict(self):
        actual = data.DataFile()
        actual.name = 'foo'
        actual.description = 'bar'
        expected = {
            'type': data.DataFile.get_type_name(),
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'description': 'bar',
            'display_name': 'Untitled Data File',
            'id': None,
            'instance_id': None,
            'invites': [],
            'modified': None,
            'name': 'foo',
            'owner': user.NONE_USER,
            'shares': [],
            'source': None,
            'source_detail': None,
            'version': None,
            'mime_type': 'application/octet-stream',
            'size': None,
            'human_readable_size': None,
            'type_display_name': 'Data File'
        }
        self.assertDictEqual(expected, actual.to_dict())

    def test_to_dict_with_nested(self):
        actual = registry.Component()
        actual.name = 'foo'
        actual.description = 'bar'
        resource = registry.Resource()
        resource.resource_type_name = folder.Folder.get_type_name()
        resource.base_path = ''
        resource.file_system_name = 'my_file_system'
        actual.resources = [resource]
        self.maxDiff = None
        expected = {
            'type': registry.Component.get_type_name(),
            'name': 'foo',
            'description': 'bar',
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'display_name': 'Untitled Registry Component',
            'id': None,
            'instance_id': None,
            'invites': [],
            'modified': None,
            'owner': user.NONE_USER,
            'shares': [],
            'source': None,
            'source_detail': None,
            'base_url': None,
            'external_base_url': None,
            'resources': [
                {
                    'type': registry.Resource.get_type_name(),
                    'resource_type_name': folder.Folder.get_type_name(),
                    'base_path': '',
                    'file_system_name': 'my_file_system',
                    'file_system_type': MongoDBFileSystem.get_type_name(),
                    'resource_collection_type_display_name': folder.Folder.get_type_name(),
                    'creator_users': [],
                    'collection_accessor_users': [],
                    'default_shares': [],
                    'type_display_name': 'Resource',
                    'manages_creators': False
                }
            ],
            'type_display_name': 'Registry Component'
        }
        self.assertDictEqual(expected, actual.to_dict())

    def test_to_dict_aws_credentials(self):
        expected = {'source', 'source_detail', 'name', 'display_name', 'description', 'owner',
                    'created', 'modified', 'derived_by', 'derived_from', 'invites',
                    'shares', 'type', 'id', 'type_display_name', 'instance_id',
                    'session_token', 'time_to_expire', 'account', 'expiration', 'role_arn',
                    'where', 'managed', 'password', 'lifespan_class', 'account_id', 'aws_role_name',
                    'lifespan', 'maximum_duration', 'state', 'role', 'temporary', 'for_presigned_url'}
        self.assertEqual(expected, set(keychain.AWSCredentials().to_dict().keys()))
