from unittest import TestCase
from heaobject import data
from datetime import datetime, timezone


class TestJSONDumps(TestCase):

    def test_json_dumps_method(self):
        expected = data.DataFile()
        expected.name = 'foo'
        expected.description = 'bar'
        expected.created = datetime(year=2020, month=1, day=1, tzinfo=timezone.utc)
        str_ = expected.to_json()
        actual = data.DataFile()
        actual.from_json(str_)
        self.assertEqual(expected.to_dict(), actual.to_dict())
