from unittest import TestCase
from heaobject.folder import AWSS3BucketItem
from s3_bucket_uri_test_util import id_name_display_name_bucket_id_tests


class TestAWSS3BucketItem(TestCase):
    def setUp(self) -> None:
        self.item = AWSS3BucketItem()


id_name_display_name_bucket_id_tests(TestAWSS3BucketItem, 'item')
