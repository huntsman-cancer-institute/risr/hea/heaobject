from unittest import TestCase
from itertools import permutations


def id_name_display_name_bucket_id_tests(cls: type[TestCase], obj: str):
    def my_test_generator(a, b):
        def test(self: TestCase):
            attr = getattr(self, obj)
            setattr(attr, a, 'foo')
            self.assertEqual(getattr(attr, b), 'foo')

        return test

    for attr_pair in permutations(['id', 'name', 'bucket_id', 'display_name']):
        _test_name = f'test_id_name_display_name_bucket_id_with_{attr_pair[0]}_{attr_pair[1]}'
        _test = my_test_generator(attr_pair[0], attr_pair[1])
        setattr(cls, _test_name, _test)
