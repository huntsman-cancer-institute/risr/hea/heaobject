from unittest import TestCase
from datetime import datetime, timedelta, timezone
from heaobject.util import make_timezone_aware, is_timezone_naive, system_timezone

class TestTimezoneNaive(TestCase):
    def test_is_timezone_naive_true(self):
        dt = datetime(2023, 1, 1, 12, 0, 0)
        self.assertTrue(is_timezone_naive(dt))

    def test_is_timezone_naive_false(self):
        tz = timezone(timedelta(hours=1))
        dt = datetime(2023, 1, 1, 12, 0, 0, tzinfo=tz)
        self.assertFalse(is_timezone_naive(dt))


class TestMakeTimezoneAware(TestCase):
    def test_make_timezone_aware_naive_is_not_naive(self):
        dt = datetime(2023, 1, 1, 12, 0, 0)
        tz = timezone(timedelta(hours=1))
        aware_dt = make_timezone_aware(dt, tz)
        self.assertFalse(is_timezone_naive(aware_dt))

    def test_make_timezone_aware_naive_correct_tz(self):
        dt = datetime(2023, 1, 1, 12, 0, 0)
        tz = timezone(timedelta(hours=1))
        aware_dt = make_timezone_aware(dt, tz)
        self.assertEqual(aware_dt.tzinfo, tz)

    def test_make_timezone_aware_already_aware_is_not_naive(self):
        tz = timezone(timedelta(hours=1))
        dt = datetime(2023, 1, 1, 12, 0, 0, tzinfo=tz)
        aware_dt = make_timezone_aware(dt, tz)
        self.assertFalse(is_timezone_naive(aware_dt))

    def test_make_timezone_aware_already_aware_unchanged(self):
        tz = timezone(timedelta(hours=1))
        dt = datetime(2023, 1, 1, 12, 0, 0, tzinfo=tz)
        aware_dt = make_timezone_aware(dt, tz)
        self.assertEqual(aware_dt, dt)

    def test_make_timezone_aware_default_tz_is_not_naive(self):
        dt = datetime(2023, 1, 1, 12, 0, 0)
        aware_dt = make_timezone_aware(dt)
        self.assertFalse(is_timezone_naive(aware_dt))

    def test_make_timezone_aware_default_tz_correct_tz(self):
        dt = datetime(2023, 1, 1, 12, 0, 0)
        aware_dt = make_timezone_aware(dt)
        self.assertEqual(aware_dt.tzinfo, system_timezone())
