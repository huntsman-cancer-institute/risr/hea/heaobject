from heaobject import root


class DataPattern(root.AbstractDesktopObject):

    def __init__(self):
        super().__init__()


class SequencePattern(DataPattern):

    def __init__(self):
        super().__init__()


class FrequencyPattern(DataPattern):

    def __init__(self):
        super().__init__()


class ValueThresholdPattern(DataPattern):

    def __init__(self):
        super().__init__()
